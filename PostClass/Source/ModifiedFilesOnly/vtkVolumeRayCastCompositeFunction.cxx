/*=========================================================================

  Program:   Visualization Toolkit
  Module:    $RCSfile: vtkVolumeRayCastCompositeFunction.cxx,v $

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkVolumeRayCastCompositeFunction.h"

#include "vtkObjectFactory.h"
#include "vtkPiecewiseFunction.h"
#include "vtkVolume.h"
#include "vtkVolumeProperty.h"
#include "vtkVolumeRayCastMapper.h"
#include <vtkMath.h>

#include <math.h>

vtkCxxRevisionMacro(vtkVolumeRayCastCompositeFunction, "$Revision: 1.1 $");
vtkStandardNewMacro(vtkVolumeRayCastCompositeFunction);

#define VTK_REMAINING_OPACITY           0.02


uint32_t vtkVolumeRayCastCompositeFunction::GetVoxelShare(uint32_t Share)
{
	printf("\nShare = %u\n", Share);
	return Share;
}




// This is the templated function that actually casts a ray and computes
// The composite value. This version uses nearest neighbor interpolation
// and does not perform shading.
template <class T>
void vtkCastRay_NN_Unshaded( T *data_ptr, vtkVolumeRayCastDynamicInfo *dynamicInfo,
                          vtkVolumeRayCastStaticInfo *staticInfo )
{
  int             value=0;
  unsigned char   *grad_mag_ptr = NULL;
  float           accum_red_intensity;
  float           accum_green_intensity;
  float           accum_blue_intensity;
  float           remaining_opacity;
  float           opacity=0.0;
  float           gradient_opacity;
  int             loop;
  int             xinc, yinc, zinc;
  int             voxel[3];
  float           ray_position[3];
  int             prev_voxel[3];
  float           *SOTF;
  float           *CTF;
  float           *GTF;
  float           *GOTF;
  int             offset;
  int             steps_this_ray = 0;
  int             grad_op_is_constant;
  float           gradient_opacity_constant;
  int             num_steps;
  float           *ray_start, *ray_increment;

  num_steps = dynamicInfo->NumberOfStepsToTake;
  ray_start = dynamicInfo->TransformedStart;
  ray_increment = dynamicInfo->TransformedIncrement;
 
  SOTF =  staticInfo->Volume->GetCorrectedScalarOpacityArray();
  CTF  =  staticInfo->Volume->GetRGBArray();
  GTF  =  staticInfo->Volume->GetGrayArray();
  GOTF =  staticInfo->Volume->GetGradientOpacityArray();

  // Get the gradient opacity constant. If this number is greater than
  // or equal to 0.0, then the gradient opacity transfer function is
  // a constant at that value, otherwise it is not a constant function
  gradient_opacity_constant = staticInfo->Volume->GetGradientOpacityConstant();
  grad_op_is_constant = ( gradient_opacity_constant >= 0.0 );

  // Move the increments into local variables
  xinc = staticInfo->DataIncrement[0];
  yinc = staticInfo->DataIncrement[1];
  zinc = staticInfo->DataIncrement[2];

  // Initialize the ray position and voxel location
  ray_position[0] = ray_start[0];
  ray_position[1] = ray_start[1];
  ray_position[2] = ray_start[2];
  voxel[0] = vtkRoundFuncMacro( ray_position[0] );
  voxel[1] = vtkRoundFuncMacro( ray_position[1] );
  voxel[2] = vtkRoundFuncMacro( ray_position[2] );

  // So far we haven't accumulated anything
  accum_red_intensity     = 0.0;
  accum_green_intensity   = 0.0;
  accum_blue_intensity    = 0.0;
  remaining_opacity       = 1.0;

  // Get a pointer to the gradient magnitudes for this volume
  if ( !grad_op_is_constant )
    {
    grad_mag_ptr = staticInfo->GradientMagnitudes;
    }

  
  // Keep track of previous voxel to know when we step into a new one
  // set it to something invalid to start with so that everything is
  // computed first time through
  prev_voxel[0] = voxel[0]-1;
  prev_voxel[1] = voxel[1]-1;
  prev_voxel[2] = voxel[2]-1;
  
  // Two cases - we are working with a gray or RGB transfer
  // function - break them up to make it more efficient
  if ( staticInfo->ColorChannels == 1 ) 
    {
    // For each step along the ray
    for ( loop = 0; 
          loop < num_steps && remaining_opacity > VTK_REMAINING_OPACITY; 
          loop++ )
      {     
      // We've taken another step
      steps_this_ray++;
      
      // Access the value at this voxel location
      if ( prev_voxel[0] != voxel[0] ||
           prev_voxel[1] != voxel[1] ||
           prev_voxel[2] != voxel[2] )
        {
        offset = voxel[2] * zinc + voxel[1] * yinc + voxel[0] * xinc;
        value = *(data_ptr + offset);
        opacity = SOTF[value];

        if ( opacity )
          {
          if ( grad_op_is_constant )
            {
            gradient_opacity = gradient_opacity_constant;
            }
          else 
            {
            gradient_opacity = GOTF[*(grad_mag_ptr + offset)];
            }
          opacity *= gradient_opacity;
          }

        prev_voxel[0] = voxel[0];
        prev_voxel[1] = voxel[1];
        prev_voxel[2] = voxel[2];
        }
        
      // Accumulate some light intensity and opacity
      accum_red_intensity   += ( opacity * remaining_opacity * 
                                 GTF[(value)] );
      remaining_opacity *= (1.0 - opacity);
      
      // Increment our position and compute our voxel location
      ray_position[0] += ray_increment[0];
      ray_position[1] += ray_increment[1];
      ray_position[2] += ray_increment[2];
      voxel[0] = vtkRoundFuncMacro( ray_position[0] );
      voxel[1] = vtkRoundFuncMacro( ray_position[1] );
      voxel[2] = vtkRoundFuncMacro( ray_position[2] );
      }
    accum_green_intensity = accum_red_intensity;
    accum_blue_intensity = accum_red_intensity;
    }
  else if ( staticInfo->ColorChannels == 3 )
    {
    // For each step along the ray
    for ( loop = 0; 
          loop < num_steps && remaining_opacity > VTK_REMAINING_OPACITY; 
          loop++ )
      {     
      // We've taken another step
      steps_this_ray++;
      
      // Access the value at this voxel location
      if ( prev_voxel[0] != voxel[0] ||
           prev_voxel[1] != voxel[1] ||
           prev_voxel[2] != voxel[2] )
        {
        offset = voxel[2] * zinc + voxel[1] * yinc + voxel[0] * xinc;
        value = *(data_ptr + offset);
        opacity = SOTF[value];

        if ( opacity )
          {
          if ( grad_op_is_constant )
            {
            gradient_opacity = gradient_opacity_constant;
            }
          else 
            {
            gradient_opacity = GOTF[*(grad_mag_ptr + offset)];  
            }
          opacity *= gradient_opacity;
          }

        prev_voxel[0] = voxel[0];
        prev_voxel[1] = voxel[1];
        prev_voxel[2] = voxel[2];
        }


      // Accumulate some light intensity and opacity
      accum_red_intensity   += ( opacity * remaining_opacity * 
                                 CTF[(value)*3] );
      accum_green_intensity += ( opacity * remaining_opacity * 
                                 CTF[(value)*3 + 1] );
      accum_blue_intensity  += ( opacity * remaining_opacity * 
                                 CTF[(value)*3 + 2] );
      remaining_opacity *= (1.0 - opacity);
      
      // Increment our position and compute our voxel location
      ray_position[0] += ray_increment[0];
      ray_position[1] += ray_increment[1];
      ray_position[2] += ray_increment[2];
      voxel[0] = vtkRoundFuncMacro( ray_position[0] );
      voxel[1] = vtkRoundFuncMacro( ray_position[1] );
      voxel[2] = vtkRoundFuncMacro( ray_position[2] );
      }
    }

  // Cap the intensity value at 1.0
  if ( accum_red_intensity > 1.0 )
    {
    accum_red_intensity = 1.0;
    }
  if ( accum_green_intensity > 1.0 )
    {
    accum_green_intensity = 1.0;
    }
  if ( accum_blue_intensity > 1.0 )
    {
    accum_blue_intensity = 1.0;
    }
  
  if( remaining_opacity < VTK_REMAINING_OPACITY )
    {
    remaining_opacity = 0.0;
    }

  // Set the return pixel value.  The depth value is the distance to the
  // center of the volume.
  dynamicInfo->Color[0] = accum_red_intensity;
  dynamicInfo->Color[1] = accum_green_intensity;
  dynamicInfo->Color[2] = accum_blue_intensity;
  dynamicInfo->Color[3] = 1.0 - remaining_opacity;
  dynamicInfo->NumberOfStepsTaken = steps_this_ray;
  
}


// This is the templated function that actually casts a ray and computes
// the composite value. This version uses nearest neighbor and does
// perform shading.
template <class T>
void vtkCastRay_NN_Shaded( T *data_ptr, vtkVolumeRayCastDynamicInfo *dynamicInfo,
                           vtkVolumeRayCastStaticInfo *staticInfo )
{
  int             value;
  unsigned char   *grad_mag_ptr = NULL;
  float           accum_red_intensity;
  float           accum_green_intensity;
  float           accum_blue_intensity;
  float           remaining_opacity;
  float           opacity = 0.0;
  float           gradient_opacity;
  int             loop;
  int             xinc, yinc, zinc;
  int             voxel[3];
  float           ray_position[3];
  int             prev_voxel[3];
  float           *SOTF;
  float           *CTF;
  float           *GTF;
  float           *GOTF;
  float           *red_d_shade, *green_d_shade, *blue_d_shade;
  float           *red_s_shade, *green_s_shade, *blue_s_shade;
  unsigned short  *encoded_normals;
  float           red_shaded_value   = 0.0;
  float           green_shaded_value = 0.0;
  float           blue_shaded_value  = 0.0;
  int             offset;
  int             steps_this_ray = 0;
  int             grad_op_is_constant;
  float           gradient_opacity_constant;
  int             num_steps;
  float           *ray_start, *ray_increment;

  num_steps = dynamicInfo->NumberOfStepsToTake;
  ray_start = dynamicInfo->TransformedStart;
  ray_increment = dynamicInfo->TransformedIncrement;
 
  // Get diffuse shading table pointers
  red_d_shade = staticInfo->RedDiffuseShadingTable;
  green_d_shade = staticInfo->GreenDiffuseShadingTable;
  blue_d_shade = staticInfo->BlueDiffuseShadingTable;

  // Get specular shading table pointers
  red_s_shade = staticInfo->RedSpecularShadingTable;
  green_s_shade = staticInfo->GreenSpecularShadingTable;
  blue_s_shade = staticInfo->BlueSpecularShadingTable;

  // Get a pointer to the encoded normals for this volume
  encoded_normals = staticInfo->EncodedNormals;

  // Get the scalar opacity transfer function for this volume (which maps
  // scalar input values to opacities)
  SOTF =  staticInfo->Volume->GetCorrectedScalarOpacityArray();

  // Get the color transfer function for this volume (which maps
  // scalar input values to RGB values)
  CTF =  staticInfo->Volume->GetRGBArray();
  GTF =  staticInfo->Volume->GetGrayArray();

  // Get the gradient opacity transfer function for this volume (which maps
  // gradient magnitudes to opacities)
  GOTF =  staticInfo->Volume->GetGradientOpacityArray();

  // Get the gradient opacity constant. If this number is greater than
  // or equal to 0.0, then the gradient opacity transfer function is
  // a constant at that value, otherwise it is not a constant function
  gradient_opacity_constant = staticInfo->Volume->GetGradientOpacityConstant();
  grad_op_is_constant = ( gradient_opacity_constant >= 0.0 );

  // Get a pointer to the gradient magnitudes for this volume
  if ( !grad_op_is_constant )
    {
    grad_mag_ptr = staticInfo->GradientMagnitudes;
    }

  // Move the increments into local variables
  xinc = staticInfo->DataIncrement[0];
  yinc = staticInfo->DataIncrement[1];
  zinc = staticInfo->DataIncrement[2];

  // Initialize the ray position and voxel location
  ray_position[0] = ray_start[0];
  ray_position[1] = ray_start[1];
  ray_position[2] = ray_start[2];

  voxel[0] = vtkRoundFuncMacro( ray_position[0] );
  voxel[1] = vtkRoundFuncMacro( ray_position[1] );
  voxel[2] = vtkRoundFuncMacro( ray_position[2] );

  // So far we haven't accumulated anything
  accum_red_intensity     = 0.0;
  accum_green_intensity   = 0.0;
  accum_blue_intensity    = 0.0;
  remaining_opacity       = 1.0;

  // Keep track of previous voxel to know when we step into a new one  
  prev_voxel[0] = voxel[0]-1;
  prev_voxel[1] = voxel[1]-1;
  prev_voxel[2] = voxel[2]-1;
  
  // Two cases - we are working with a gray or RGB transfer
  // function - break them up to make it more efficient
  if ( staticInfo->ColorChannels == 1 ) 
    {
    // For each step along the ray
    for ( loop = 0; 
          loop < num_steps && remaining_opacity > VTK_REMAINING_OPACITY; 
          loop++ )
      {     
      // We've taken another step
      steps_this_ray++;
      
      // Access the value at this voxel location and compute
      // opacity and shaded value
      if ( prev_voxel[0] != voxel[0] ||
           prev_voxel[1] != voxel[1] ||
           prev_voxel[2] != voxel[2] )
        {
        offset = voxel[2] * zinc + voxel[1] * yinc + voxel[0] * xinc;
        value = *(data_ptr + offset);
      
        // Get the opacity contributed by the scalar opacity transfer function
        opacity = SOTF[value];

        // Multiply by the opacity contributed by the gradient magnitude
        // transfer function (don't both if opacity is already 0)
        if ( opacity )
          {
          if ( grad_op_is_constant )
            {
            gradient_opacity = gradient_opacity_constant;
            }
          else 
            {
            gradient_opacity = GOTF[*(grad_mag_ptr + offset)];
            }
          
          opacity *= gradient_opacity;
          
          }

        // Compute the red shaded value (only if there is some opacity)
        // This is grey-scale so green and blue are the same as red
        if ( opacity )
          {
          red_shaded_value = opacity * remaining_opacity *
            ( red_d_shade[*(encoded_normals + offset)] * GTF[value] +
              red_s_shade[*(encoded_normals + offset)] );
          }
        else
          {
          red_shaded_value = 0.0;
          }

        prev_voxel[0] = voxel[0];
        prev_voxel[1] = voxel[1];
        prev_voxel[2] = voxel[2];
        }
    
    
      // Accumulate the shaded intensity and opacity of this sample
      accum_red_intensity += red_shaded_value;
      remaining_opacity *= (1.0 - opacity);
    
      // Increment our position and compute our voxel location
      ray_position[0] += ray_increment[0];
      ray_position[1] += ray_increment[1];
      ray_position[2] += ray_increment[2];
      voxel[0] = vtkRoundFuncMacro( ray_position[0] );
      voxel[1] = vtkRoundFuncMacro( ray_position[1] );
      voxel[2] = vtkRoundFuncMacro( ray_position[2] );
      }
    accum_green_intensity = accum_red_intensity;
    accum_blue_intensity = accum_red_intensity;
    }
  else if ( staticInfo->ColorChannels == 3 )
    {
    // For each step along the ray
    for ( loop = 0; 
          loop < num_steps && remaining_opacity > VTK_REMAINING_OPACITY; loop++ )
      {     
      // We've taken another step
      steps_this_ray++;
      
      // Access the value at this voxel location and compute
      // opacity and shaded value
      if ( prev_voxel[0] != voxel[0] ||
           prev_voxel[1] != voxel[1] ||
           prev_voxel[2] != voxel[2] )
        {
        offset = voxel[2] * zinc + voxel[1] * yinc + voxel[0] * xinc;
        value = *(data_ptr + offset);
      
        // Get the opacity contributed by the scalar opacity transfer function
        opacity = SOTF[value];

        // Multiply by the opacity contributed by the gradient magnitude
        // transfer function (don't both if opacity is already 0)
        if ( opacity )
          {
          if ( grad_op_is_constant )
            {
            gradient_opacity = gradient_opacity_constant;
            }
          else 
            {
            gradient_opacity = GOTF[*(grad_mag_ptr + offset)];
            }
          
          opacity *= gradient_opacity;    
          }     

        // Compute the red, green, and blue shaded value (only if there
        // is some opacity)
        if ( opacity )
          {
          red_shaded_value = opacity *  remaining_opacity *
            ( red_d_shade[*(encoded_normals + offset)] * CTF[value*3] +
              red_s_shade[*(encoded_normals + offset)] );
          green_shaded_value = opacity *  remaining_opacity *
            ( green_d_shade[*(encoded_normals + offset)] * CTF[value*3 + 1] +
              green_s_shade[*(encoded_normals + offset)] );
          blue_shaded_value = opacity *  remaining_opacity *
            ( blue_d_shade[*(encoded_normals + offset)] * CTF[value*3 + 2] +
              blue_s_shade[*(encoded_normals + offset)] );
          }
        else
          {
          red_shaded_value = 0.0;
          green_shaded_value = 0.0;
          blue_shaded_value = 0.0;
          }

        prev_voxel[0] = voxel[0];
        prev_voxel[1] = voxel[1];
        prev_voxel[2] = voxel[2];
        }
    
    
      // Accumulate the shaded intensity and opacity of this sample
      accum_red_intensity += red_shaded_value;
      accum_green_intensity += green_shaded_value;
      accum_blue_intensity += blue_shaded_value;
      remaining_opacity *= (1.0 - opacity);
    
      // Increment our position and compute our voxel location
      ray_position[0] += ray_increment[0];
      ray_position[1] += ray_increment[1];
      ray_position[2] += ray_increment[2];
      voxel[0] = vtkRoundFuncMacro( ray_position[0] );
      voxel[1] = vtkRoundFuncMacro( ray_position[1] );
      voxel[2] = vtkRoundFuncMacro( ray_position[2] );
      }
    }
  
  // Cap the intensities at 1.0
  if ( accum_red_intensity > 1.0 )
    {
    accum_red_intensity = 1.0;
    }
  if ( accum_green_intensity > 1.0 )
    {
    accum_green_intensity = 1.0;
    }
  if ( accum_blue_intensity > 1.0 )
    {
    accum_blue_intensity = 1.0;
    }
  
  if( remaining_opacity < VTK_REMAINING_OPACITY )
    {
    remaining_opacity = 0.0;
    }
  
  // Set the return pixel value.  The depth value is the distance to the
  // center of the volume.
  dynamicInfo->Color[0] = accum_red_intensity;
  dynamicInfo->Color[1] = accum_green_intensity;
  dynamicInfo->Color[2] = accum_blue_intensity;
  dynamicInfo->Color[3] = 1.0 - remaining_opacity;
  dynamicInfo->NumberOfStepsTaken = steps_this_ray;
  
}

// This is the templated function that actually casts a ray and computes
// the composite value.  This version uses trilinear interpolation and
// does not compute shading
template <class T>
void vtkCastRay_TrilinSample_Unshaded( T *data_ptr, vtkVolumeRayCastDynamicInfo *dynamicInfo,
                                       vtkVolumeRayCastStaticInfo *staticInfo )
{
  //SecretShare M;
  unsigned char   *grad_mag_ptr = NULL;
  unsigned char   *gmptr;
  float           accum_red_intensity;
  float           accum_green_intensity;
  float           accum_blue_intensity;
  float           remaining_opacity;
  float           red_value, green_value, blue_value;
  float           opacity;
  int             loop;
  int             xinc, yinc, zinc;
  int             voxel[3];
  float           ray_position[3];
  float           A, B, C, D, E, F, G, H;
  int             Binc, Cinc, Dinc, Einc, Finc, Ginc, Hinc;
  T               *dptr;
  float           *SOTF;
  float           *CTF;
  float           *GTF;
  float           *GOTF;
  float           x, y, z, t1, t2, t3;
  int             offset;
  int             steps_this_ray = 0;
  float           gradient_value;
  float           scalar_value;
  int             grad_op_is_constant;
  float           gradient_opacity_constant;
  int             num_steps;
  float           *ray_start, *ray_increment;
  float temp;
  int t, k;
  float accum_opacity;

  int x1=25;	

  uint32_t tt = 0;
 /* tt  = GetVoxelShare(tt);*/

  num_steps = dynamicInfo->NumberOfStepsToTake;
  ray_start = dynamicInfo->TransformedStart;
  ray_increment = dynamicInfo->TransformedIncrement;

  // Get the scalar opacity transfer function which maps scalar input values
  // to opacities
  SOTF =  staticInfo->Volume->GetCorrectedScalarOpacityArray();

  // Get the color transfer function which maps scalar input values
  // to RGB colors
  CTF =  staticInfo->Volume->GetRGBArray();
  GTF =  staticInfo->Volume->GetGrayArray();

  // Get the gradient opacity transfer function for this volume (which maps
  // gradient magnitudes to opacities)
  GOTF =  staticInfo->Volume->GetGradientOpacityArray();

  // Get the gradient opacity constant. If this number is greater than
  // or equal to 0.0, then the gradient opacity transfer function is
  // a constant at that value, otherwise it is not a constant function
  gradient_opacity_constant = staticInfo->Volume->GetGradientOpacityConstant();
  grad_op_is_constant = ( gradient_opacity_constant >= 0.0 );

  // Get a pointer to the gradient magnitudes for this volume
  if ( !grad_op_is_constant )
    {
    grad_mag_ptr = staticInfo->GradientMagnitudes;
    }

  // Move the increments into local variables
  xinc = staticInfo->DataIncrement[0];
  yinc = staticInfo->DataIncrement[1];
  zinc = staticInfo->DataIncrement[2];
 // printf("\nxinc = %d yinc = %d zinc = %d\n", xinc, yinc, zinc);

  // Initialize the ray position and voxel location
  ray_position[0] = ray_start[0];
  ray_position[1] = ray_start[1];
  ray_position[2] = ray_start[2];
  voxel[0] = vtkFloorFuncMacro( ray_position[0] );
  voxel[1] = vtkFloorFuncMacro( ray_position[1] );
  voxel[2] = vtkFloorFuncMacro( ray_position[2] );

  // So far we have not accumulated anything
  accum_red_intensity     = 0.0;
  accum_green_intensity   = 0.0;
  accum_blue_intensity    = 0.0;
  remaining_opacity       = 1.0;

  // Compute the increments to get to the other 7 voxel vertices from A
  Binc = xinc;
  Cinc = yinc;
  Dinc = xinc + yinc;
  Einc = zinc;
  Finc = zinc + xinc;
  Ginc = zinc + yinc;
  Hinc = zinc + xinc + yinc;
  
  // Two cases - we are working with a gray or RGB transfer
  // function - break them up to make it more efficient
  if ( staticInfo->ColorChannels == 1 ) 
    {
    // For each step along the ray
    for ( loop = 0; 
          loop < num_steps && remaining_opacity > VTK_REMAINING_OPACITY; 
          loop++ )
      {     
      // We've taken another step
      steps_this_ray++;
      
      offset = voxel[2] * zinc + voxel[1] * yinc + voxel[0];
      dptr = data_ptr + offset;
      
      A = *(dptr);
      B = *(dptr + Binc);
      C = *(dptr + Cinc);
      D = *(dptr + Dinc);
      E = *(dptr + Einc);
      F = *(dptr + Finc);
      G = *(dptr + Ginc);
      H = *(dptr + Hinc);
      
      // Compute our offset in the voxel, and use that to trilinearly
      // interpolate the value
      x = ray_position[0] - (float) voxel[0];
      y = ray_position[1] - (float) voxel[1];
      z = ray_position[2] - (float) voxel[2];
      
      t1 = 1.0 - x;
      t2 = 1.0 - y;
      t3 = 1.0 - z;
      
      scalar_value = 
        A * t1 * t2 * t3 +
        B *  x * t2 * t3 +
        C * t1 *  y * t3 + 
        D *  x *  y * t3 +
        E * t1 * t2 *  z + 
        F *  x * t2 *  z + 
        G * t1 *  y *  z + 
        H *  x *  y *  z;
      
      if ( scalar_value < 0.0 ) 
        {
        scalar_value = 0.0;
        }
      else if ( scalar_value > staticInfo->Volume->GetArraySize() - 1 )
        {
        scalar_value = staticInfo->Volume->GetArraySize() - 1;
        }
      
      opacity = SOTF[(int)(scalar_value)];
      
      if ( opacity )
        {
        if ( !grad_op_is_constant )
          {
          gmptr = grad_mag_ptr + offset;
      
          A = *(gmptr);
          B = *(gmptr + Binc);
          C = *(gmptr + Cinc);
          D = *(gmptr + Dinc);
          E = *(gmptr + Einc);
          F = *(gmptr + Finc);
          G = *(gmptr + Ginc);
          H = *(gmptr + Hinc);
          
          gradient_value = 
            A * t1 * t2 * t3 +
            B *  x * t2 * t3 +
            C * t1 *  y * t3 + 
            D *  x *  y * t3 +
            E * t1 * t2 *  z + 
            F *  x * t2 *  z + 
            G * t1 *  y *  z + 
            H *  x *  y *  z;
      
          if ( gradient_value < 0.0 )
            {
            gradient_value = 0.0;
            }
          else if ( gradient_value > 255.0 )
            {
            gradient_value = 255.0;
            }

          opacity *= GOTF[(int)(gradient_value)];
          }
        else
          {
          opacity *= gradient_opacity_constant;
          }
        red_value   = opacity * GTF[(int)(scalar_value)];
        
        // Accumulate intensity and opacity for this sample location
        accum_red_intensity   += remaining_opacity * red_value;
        remaining_opacity *= (1.0 - opacity);
        }
    
      // Increment our position and compute our voxel location
      ray_position[0] += ray_increment[0];
      ray_position[1] += ray_increment[1];
      ray_position[2] += ray_increment[2];      
      voxel[0] = vtkFloorFuncMacro( ray_position[0] );
      voxel[1] = vtkFloorFuncMacro( ray_position[1] );
      voxel[2] = vtkFloorFuncMacro( ray_position[2] );
      }
    accum_green_intensity = accum_red_intensity;
    accum_blue_intensity = accum_red_intensity;
    }
  else if ( staticInfo->ColorChannels == 3 )
    {
    // For each step along the ray
    // printf("\nNumber of steps = %d", num_steps);
    for ( loop = 0; 
          loop < num_steps && remaining_opacity > VTK_REMAINING_OPACITY; 
          loop++ )
      {     
      // We've taken another step
      steps_this_ray++;
      
      offset = voxel[2] * zinc + voxel[1] * yinc + voxel[0];
      dptr = data_ptr + offset;
      
      A = *(dptr);
      B = *(dptr + Binc);
      C = *(dptr + Cinc);
      D = *(dptr + Dinc);
      E = *(dptr + Einc);
      F = *(dptr + Finc);
      G = *(dptr + Ginc);
      H = *(dptr + Hinc);
      
      // Compute our offset in the voxel, and use that to trilinearly
      // interpolate the value
      x = ray_position[0] - (float) voxel[0];
      y = ray_position[1] - (float) voxel[1];
      z = ray_position[2] - (float) voxel[2];
      
      t1 = 1.0 - x;
      t2 = 1.0 - y;
      t3 = 1.0 - z;
      
      scalar_value = 
        A * t1 * t2 * t3 +
        B *  x * t2 * t3 +
        C * t1 *  y * t3 + 
        D *  x *  y * t3 +
        E * t1 * t2 *  z + 
        F *  x * t2 *  z + 
        G * t1 *  y *  z + 
        H *  x *  y *  z;
      
      if ( scalar_value < 0.0 ) 
        {
        scalar_value = 0.0;
        }
      else if ( scalar_value > staticInfo->Volume->GetArraySize() - 1 )
        {
        scalar_value = staticInfo->Volume->GetArraySize() - 1;
        }
      
      opacity = SOTF[(int)(scalar_value)];
    //  opacity = opacity + 0.123;
      if(opacity > 1.0)
	opacity = opacity - 1.0;
      
      if ( opacity )
        {
        if ( !grad_op_is_constant )
          {
          gmptr = grad_mag_ptr + offset;
          
          A = *(gmptr);
          B = *(gmptr + Binc);
          C = *(gmptr + Cinc);
          D = *(gmptr + Dinc);
          E = *(gmptr + Einc);
          F = *(gmptr + Finc);
          G = *(gmptr + Ginc);
          H = *(gmptr + Hinc);
          
          gradient_value = 
            A * t1 * t2 * t3 +
            B *  x * t2 * t3 +
            C * t1 *  y * t3 + 
            D *  x *  y * t3 +
            E * t1 * t2 *  z + 
            F *  x * t2 *  z + 
            G * t1 *  y *  z + 
            H *  x *  y *  z;
          
          if ( gradient_value < 0.0 )
            {
            gradient_value = 0.0;
            }
          else if ( gradient_value > 255.0 )
            {
            gradient_value = 255.0;
            }

        //  opacity *= GOTF[(int)(gradient_value)];
          }
        else
          {
         // opacity *= gradient_opacity_constant;
          }

        red_value   = opacity *  ( CTF[(int)(scalar_value) * 3 ] + 0);
	red_value = red_value * 255;
	
	t = int(red_value);
	k = t;
 	t = k + 65*x1 + 123*x1*x1;
	t = t % 256;
	red_value = float(t);      

        green_value = opacity * ( CTF[(int)(scalar_value) * 3 + 1] + 0);
	green_value = green_value * 255;

	t = int(green_value);
	k = t;
 	t = k + 222*x1 + 87*x1*x1;
	t = t % 256;
	green_value = float(t);
	      

        blue_value  = opacity * ( CTF[(int)(scalar_value) * 3 + 2] + 0);
	blue_value = blue_value * 255;

	t = int(blue_value);
	k = t;
 	t = k + 25*x1 + 322*x1*x1;
	t = t % 256;
	blue_value = float(t);		
        
        // Accumulate intensity and opacity for this sample location
	
	temp = remaining_opacity * red_value;
	accum_red_intensity   += temp;

	temp = remaining_opacity * green_value;
	accum_green_intensity += temp;

	temp = remaining_opacity * blue_value;
	accum_blue_intensity  += temp;

	remaining_opacity *= (1.0 - opacity);
	accum_opacity = remaining_opacity;
        }
    
      
      // Increment our position and compute our voxel location
      ray_position[0] += ray_increment[0];
      ray_position[1] += ray_increment[1];
      ray_position[2] += ray_increment[2]; 
      	     
      voxel[0] = vtkFloorFuncMacro( ray_position[0] );
      voxel[1] = vtkFloorFuncMacro( ray_position[1] );
      voxel[2] = vtkFloorFuncMacro( ray_position[2] );
      }
    }

   //printf("\nStep this ray has taken = %d\n", steps_this_ray);
  // Cap the intensity value at 1.0
  /*if ( accum_red_intensity > 1.0 )
    {
    accum_red_intensity = 1.0;
    }
  if ( accum_green_intensity > 1.0 )
    {
    accum_green_intensity = 1.0;
    }
  if ( accum_blue_intensity > 1.0 )
    {
    accum_blue_intensity = 1.0;
    }
  
  if( remaining_opacity < VTK_REMAINING_OPACITY )
    {
    remaining_opacity = 0.0;
    }*/

  // Set the return pixel value.  The depth value is the distance to the
  // center of the volume.
  dynamicInfo->Color[0] = accum_red_intensity;
  dynamicInfo->Color[1] = accum_green_intensity;
  //dynamicInfo->Color[2] = accum_blue_intensity;
  dynamicInfo->Color[3] = 1.0 - remaining_opacity;
 // dynamicInfo->Color[3] = 1.0 - accum_opacity;
  dynamicInfo->NumberOfStepsTaken = steps_this_ray;
}


/*Secret Sharing Modules*/

//Find Lagrange Basis Function
double FindXtermLC(int i, long int x0, long int x1, long int x2)
{
	double X;
	if(i == 0)
	{
		X = ( (double) (0 - x1) / (x0 - x1) ) * ( (double) (0 - x2) / (x0 - x2) );
	}
	else if ( i == 1)
	{
		X = ( (double) (0 - x0) / (x1 - x0) ) * ( (double) (0 - x2) / (x1 - x2));
	}
	else if ( i == 2)
	{
		X = ( (double) (0 - x0) / (x2 - x0) ) * ( (double) (0 - x1) / (x2 - x1));
	}	
	return X;
}

//Function comparing whether f1 == f2, where both f1 and f2 are floats
int compare_longdoubleC(long double f1, long double f2)
{
  	long double precision = 0.000001;
 	if (((f1 - precision) < f2) &&  ((f1 + precision) > f2))
   	{
   		 return 1;
   	}
 	else
  	{
    		return 0;
   	}
 }

//Function finding the modular prime value of a float
uint64_t ModP64C(long double number, uint64_t p)
{
	
	int64_t intPart = 0;
	long double fracPart = 0;
	uint64_t val=0;
	int64_t temp, temp1;
	int64_t modInt, modFrac, i;

	intPart = (int64_t) (number);
	fracPart = number - intPart;	
	if(fracPart == 0.0)
	{
		if(intPart < 0)
		{
			for(i=1; (intPart + i* (int64_t) p) < 0; i++);
			modInt = intPart + i*p;
		}
		else
			modInt = intPart % p;
		
		return modInt;
	}

	int sign;

	if(number < 0)
		sign = -1;
	else
		sign = 1;
	number = number * sign;
	intPart = number;
	fracPart = number - intPart;	
	
	int flag = 1;	
	for(temp = 1; flag; temp++)
	{
		for(temp1=1; temp1<temp && flag; temp1++)
		{
			long double a = (long double) temp1 / (long double) temp;
			long double b = fracPart;
			if (compare_longdoubleC(a, b))
				flag = 0;
		}
	}

	temp --;
	temp1 --;	// find a/b
	intPart = intPart * temp + temp1;
	intPart = intPart * sign;
	if(intPart < 0)
	{
		for(i=1; intPart + i*(int)p < 0; i++);
		modInt = intPart + i*p;
	}
	else
		modInt = intPart % p;

	for(i=1; ((temp*i - 1) % p) != 0; i++);
	modFrac = i;
	val = modFrac * modInt;
	val = val % p;
	return val;

}

/*Finding secret value using Lagrange interpolation*/
uint64_t FindSecretFromShare64ModPC(long int x[], uint64_t y[], uint64_t p)
{
	long double Secret = 0.0;
	uint64_t S;
	int i;
	for(i=0; i<3; i++)
	{
		Secret = Secret + y[i] * FindXtermLC(i, x[0], x[1], x[2]);				
	}
	S = ModP64C(Secret, p);
	return S;
}

/*The main module being chnaged to accomadate secret sharing*/

// This is the templated function that actually casts a ray and computes
// the composite value.  This version uses trilinear interpolation, and
// does perform shading.
template <class T>
void vtkCastRay_TrilinSample_Shaded( T *data_ptr, vtkVolumeRayCastDynamicInfo *dynamicInfo,
                                     vtkVolumeRayCastStaticInfo *staticInfo )
{
	unsigned char   *grad_mag_ptr = NULL;
	unsigned char   *gmptr;
	float           accum_red_intensity;
	float           accum_green_intensity;
	float           accum_blue_intensity;
	float           remaining_opacity;
	float           opacity;
	int             loop;
	int             xinc, yinc, zinc;
	int             voxel[3];
	float           ray_position[3];
	float           A, B, C, D, E, F, G, H;
	uint64_t	  AI, BI, CI, DI, EI, FI, GI, HI;	 
	uint64_t	  AF[5], BF[5], CF[5], DF[5], EF[5], FF[5], GF[5], HF[5]; 	
	char 		  AC, BC, CC, DC, EC, FC, GC, HC; 
	int             A_n, B_n, C_n, D_n, E_n, F_n, G_n, H_n;
	float           final_rd, final_gd, final_bd;
	float           final_rs, final_gs, final_bs;
	int             Binc, Cinc, Dinc, Einc, Finc, Ginc, Hinc;
	T               *dptr;
	float           *SOTF;
	float           *CTF;
	float           *GTF;
	float           *GOTF;
	float           x, y, z, t1, t2, t3;
	float           tA, tB, tC, tD, tE, tF, tG, tH;
	float           *red_d_shade, *green_d_shade, *blue_d_shade;
	float           *red_s_shade, *green_s_shade, *blue_s_shade;
	unsigned short  *encoded_normals, *nptr;
	float           red_shaded_value, green_shaded_value, blue_shaded_value;
	int             offset;
	int             steps_this_ray = 0;
	int             gradient_value;
	int             scalar_value;
	float           r, g, b;
	int             grad_op_is_constant;
	float           gradient_opacity_constant;
	int             num_steps;
	float           *ray_start, *ray_increment;
	int Range;	
	int ctot = 0;
	//For Share
	float ShareR[5], ShareG[5], ShareB[5];
	int l;
	float  opacityS[5], remaining_opacityS[5];

  	uint64_t accmR[5], accmG[5], accmB[5];
    	uint64_t	  scalar_value_c[5];
	uint64_t	  gradient_value_c[5];

	uint8_t 	  CTF_MyR1[5][256*1000];
	uint8_t 	  CTF_MyG1[5][256*1000];
	uint8_t 	  CTF_MyB1[5][256*1000];
  	int i, j; 		
     
  	int b1, c1, b2, c2;

	uint64_t mult = 1000;
        uint64_t multD = 10000;
	uint64_t multS = 1000000;
	   
	uint8_t R1[5], G1[5], B1[5];
	  	
	Range = 255;
	  
	uint64_t qS = (uint64_t) 2560000000000013;
	uint64_t multQ = (uint64_t) 256000000000001;
	int aS=5, bS=7;

	uint64_t rd_u[8], gd_u[8], bd_u[8], rs_u[8], gs_u[8], bs_u[8];
	uint64_t rd_uS[5][8], gd_uS[5][8], bd_uS[5][8], rs_uS[5][8], gs_uS[5][8], bs_uS[5][8];
	uint64_t frd_u[5], fgd_u[5], fbd_u[5], frs_u[5], fgs_u[5], fbs_u[5];

	int xS=1, iS;
	float tempFS=0;			
	float addMe = 0.000000;

	uint64_t RO1;
	uint64_t TR[5], TG[5], TB[5];
	float MyOiF1[5], MyOiF2, TRF[5], TGF[5], TBF[5];
        uint64_t MyOint1, TempM, MyOint2, TempM1, TempM2; 
        unsigned int seed;			
          
	              
	for(i=0; i<5; i++)
	{
		TR[i] = 0;
		TG[i] = 0;
		TB[i] = 0;
		TRF[i] = 0;
		TGF[i] = 0;
		TBF[i] = 0;
	}	

 	num_steps = dynamicInfo->NumberOfStepsToTake;
  	ray_start = dynamicInfo->TransformedStart;
  	ray_increment = dynamicInfo->TransformedIncrement;

  	// Get diffuse shading table pointers
  	red_d_shade = staticInfo->RedDiffuseShadingTable;
  	green_d_shade = staticInfo->GreenDiffuseShadingTable;
  	blue_d_shade = staticInfo->BlueDiffuseShadingTable;


  	// Get diffuse shading table pointers
  	red_s_shade = staticInfo->RedSpecularShadingTable;
  	green_s_shade = staticInfo->GreenSpecularShadingTable;
  	blue_s_shade = staticInfo->BlueSpecularShadingTable;

  	//printf("\nHere: By Mano ---\n"); 
  	// Get a pointer to the encoded normals for this volume
  	encoded_normals = staticInfo->EncodedNormals;

  	// Get the scalar opacity transfer function which maps scalar input values
  	// to opacities
  	SOTF =  staticInfo->Volume->GetCorrectedScalarOpacityArray();

  	// Get the color transfer function which maps scalar input values
  	// to RGB values
  	CTF =  staticInfo->Volume->GetRGBArray();
  	GTF =  staticInfo->Volume->GetGrayArray();
  	
  	b1 = 100;
  	c1 = 100;
  	b2 = 50;
  	c2 = 70;

    	/*Copy the colors to the datacenters in SGD*/
  	for(i=0; i<256; i++)
  	{
		for(j=0; j<5; j++)
		{
			CTF_MyR1[j][i] = CTF[i * 3    ] * Range;
       			CTF_MyG1[j][i] = CTF[i * 3 + 1] * Range;
       			CTF_MyB1[j][i] = CTF[i * 3 + 2] * Range;			
		}
  	}

  	// Get the gradient opacity transfer function for this volume (which maps
  	// gradient magnitudes to opacities)
  	GOTF =  staticInfo->Volume->GetGradientOpacityArray();

  	// Get the gradient opacity constant. If this number is greater than
  	// or equal to 0.0, then the gradient opacity transfer function is
  	// a constant at that value, otherwise it is not a constant function
  	gradient_opacity_constant = staticInfo->Volume->GetGradientOpacityConstant();
  	grad_op_is_constant = ( gradient_opacity_constant >= 0.0 );

  	// Get a pointer to the gradient magnitudes for this volume
  	if ( !grad_op_is_constant )
    	{
    		grad_mag_ptr = staticInfo->GradientMagnitudes;
    	}

  	// Move the increments into local variables
  	xinc = staticInfo->DataIncrement[0];
  	yinc = staticInfo->DataIncrement[1];
  	zinc = staticInfo->DataIncrement[2];

  	// Initialize the ray position and voxel location
  	ray_position[0] = ray_start[0];
  	ray_position[1] = ray_start[1];
  	ray_position[2] = ray_start[2];
  	voxel[0] = vtkFloorFuncMacro( ray_position[0] );
  	voxel[1] = vtkFloorFuncMacro( ray_position[1] );
  	voxel[2] = vtkFloorFuncMacro( ray_position[2] );

  	// So far we haven't accumulated anything
  	accum_red_intensity   = 0.0;
  	accum_green_intensity = 0.0;
  	accum_blue_intensity  = 0.0;
  	remaining_opacity     = 1.0;

  	for(l=0; l<5; l++)
  	{
	 	accmR[l]= 0;
	 	accmG[l]= 0;
		accmB[l]= 0;
		remaining_opacityS[l] = 1.0;
 	}
   	

  	// Compute the increments to get to the other 7 voxel vertices from A
  	Binc = xinc;
  	Cinc = yinc;
  	Dinc = xinc + yinc;
  	Einc = zinc;
  	Finc = zinc + xinc;
  	Ginc = zinc + yinc;
  	Hinc = zinc + xinc + yinc;
  
  	// Two cases - we are working with a gray or RGB transfer
  	// function - break them up to make it more efficient
  	if ( staticInfo->ColorChannels == 1 ) 
    	{
    		// For each step along the ray
    		for ( loop = 0; loop < num_steps; loop++ )//&& remaining_opacity > VTK_REMAINING_OPACITY; loop++ )
      		{     
      			// We've taken another step
      			steps_this_ray++;
      
      			offset = voxel[2] * zinc + voxel[1] * yinc + voxel[0];
      			dptr = data_ptr + offset;
      			nptr = encoded_normals + offset;
    
      			A = *(dptr);
      			B = *(dptr + Binc);
      			C = *(dptr + Cinc);
      			D = *(dptr + Dinc);
      			E = *(dptr + Einc);
      			F = *(dptr + Finc);
      			G = *(dptr + Ginc);
      			H = *(dptr + Hinc);
      
      			// Compute our offset in the voxel, and use that to trilinearly
      			// interpolate a value
      			x = ray_position[0] - (float) voxel[0];
      			y = ray_position[1] - (float) voxel[1];
      			z = ray_position[2] - (float) voxel[2];
      
      			t1 = 1.0 - x;
      			t2 = 1.0 - y;
      			t3 = 1.0 - z;
      
      			tA = t1 * t2 * t3;
      			tB =  x * t2 * t3;
      			tC = t1 *  y * t3;
      			tD =  x *  y * t3;
      			tE = t1 * t2 *  z;
      			tF =  x * t2 *  z;
      			tG = t1 *  y *  z;
      			tH =  x *  y *  z;
      
      			scalar_value = (int) (
                            A * tA + B * tB + C * tC + D * tD + 
                            E * tE + F * tF + G * tG + H * tH );
      
      			if ( scalar_value < 0 ) 
        		{
        			scalar_value = 0;
        		}
      			else if ( scalar_value > staticInfo->Volume->GetArraySize() - 1 )
        		{
        			scalar_value = (int)(staticInfo->Volume->GetArraySize() - 1);
        		}
      
      			opacity = SOTF[scalar_value];

      			// If we have some opacity based on the scalar value transfer function,
      			// then multiply by the opacity from the gradient magnitude transfer
      			// function
      			if ( opacity )
        		{
        			if ( !grad_op_is_constant )
          			{
          				gmptr = grad_mag_ptr + offset;
      
          				A = *(gmptr);
          				B = *(gmptr + Binc);
          				C = *(gmptr + Cinc);
          				D = *(gmptr + Dinc);
          				E = *(gmptr + Einc);
          				F = *(gmptr + Finc);
          				G = *(gmptr + Ginc);
          				H = *(gmptr + Hinc);
          
          				gradient_value = (int) (A * tA + B * tB + C * tC + D * tD + E * tE + F * tF + G * tG + H * tH );
          				if ( gradient_value < 0 )
            				{
            					gradient_value = 0;
            				}
          				else if ( gradient_value > 255 )
            				{
            					gradient_value = 255;
            				}
          
          				opacity *= GOTF[gradient_value];
          			}
        			else
          			{
          				opacity *= gradient_opacity_constant;
          			}
        		}

      			// If we have a combined opacity value, then compute the shading
      			if ( opacity )
        		{
        			A_n = *(nptr);
        			B_n = *(nptr + Binc);
        			C_n = *(nptr + Cinc);
        			D_n = *(nptr + Dinc);
        			E_n = *(nptr + Einc);
        			F_n = *(nptr + Finc);
        			G_n = *(nptr + Ginc);
        			H_n = *(nptr + Hinc);

        			final_rd = 
          				red_d_shade[ A_n ] * tA + red_d_shade[ B_n ] * tB +   
          				red_d_shade[ C_n ] * tC + red_d_shade[ D_n ] * tD + 
          				red_d_shade[ E_n ] * tE + red_d_shade[ F_n ] * tF +   
          				red_d_shade[ G_n ] * tG + red_d_shade[ H_n ] * tH;
        
        			final_rs = 
          				red_s_shade[ A_n ] * tA + red_s_shade[ B_n ] * tB +   
          				red_s_shade[ C_n ] * tC + red_s_shade[ D_n ] * tD + 
          				red_s_shade[ E_n ] * tE + red_s_shade[ F_n ] * tF +   
          				red_s_shade[ G_n ] * tG + red_s_shade[ H_n ] * tH;
        
        			r = GTF[(scalar_value)];

        			// For this sample we have do not yet have any opacity or
        			// shaded intensity yet
        			red_shaded_value   = opacity * ( final_rd * r + final_rs );
        
        			// Accumulate intensity and opacity for this sample location   
        			accum_red_intensity   += red_shaded_value * remaining_opacity;
        			remaining_opacity *= (1.0 - opacity);
        		}

      			// Increment our position and compute our voxel location
      			ray_position[0] += ray_increment[0];
      			ray_position[1] += ray_increment[1];
      			ray_position[2] += ray_increment[2];      
      			voxel[0] = vtkFloorFuncMacro( ray_position[0] );
      			voxel[1] = vtkFloorFuncMacro( ray_position[1] );
      			voxel[2] = vtkFloorFuncMacro( ray_position[2] );
      		}
    		accum_green_intensity = accum_red_intensity;
    		accum_blue_intensity = accum_red_intensity;
    	}


//=================================================  CHANGES FOR SECRET SHARING ARE HERE ===========================================================
	
	else if ( staticInfo->ColorChannels == 3 )
  	{

    		// For each step along the ray
    		for ( loop = 0; loop < num_steps && remaining_opacity > VTK_REMAINING_OPACITY; 	loop++ )
      		{    
			// We've taken another step
      			steps_this_ray++;
      
      			offset = voxel[2] * zinc + voxel[1] * yinc + voxel[0];
      			dptr = data_ptr + offset;
      			nptr = encoded_normals + offset;
    
      			A = *(dptr);
      			B = *(dptr + Binc);
      			C = *(dptr + Cinc);
      			D = *(dptr + Dinc);
      			E = *(dptr + Einc);
      			F = *(dptr + Finc);
      			G = *(dptr + Ginc);
      			H = *(dptr + Hinc);
      
      			// Compute our offset in the voxel, and use that to trilinearly
      			// interpolate a value
     			x = ray_position[0] - (float) voxel[0];
      			y = ray_position[1] - (float) voxel[1];
      			z = ray_position[2] - (float) voxel[2];
      
			//Get interpolation factors      			
			t1 = 1.0 - x;
     	 		t2 = 1.0 - y;
      			t3 = 1.0 - z;
      
      			tA = t1 * t2 * t3;
      			tB =  x * t2 * t3;
      			tC = t1 *  y * t3;
      			tD =  x *  y * t3;
      			tE = t1 * t2 *  z;
      			tF =  x * t2 *  z;
      			tG = t1 *  y *  z;
      			tH =  x *  y *  z;
      
			//Interpolated Scalar value in the conventional rendering scheme
			scalar_value = (int) (A * tA + B * tB + C * tC + D * tD + E * tE + F * tF + G * tG + H * tH );
				
			
			//Convert floating point scalar values of eight neighbouring voxels of the sample point 
			//to integers [roundoff, and mult decimal places by 10^mult]
			AI = (A+addMe)*mult;
			BI = (B+addMe)*mult;
			CI = (C+addMe)*mult;
			DI = (D+addMe)*mult;
			EI = (E+addMe)*mult;
			FI = (F+addMe)*mult;
			GI = (G+addMe)*mult;
			HI = (H+addMe)*mult;
			
			//Secret share scalar values and interpolate

			//Obtain random numbers as required for secret sharing
			uint64_t a, b;
			a = rand() % multQ;
			b = rand() % multQ;	
			
			for(i=0; i<5; i++)
			{
				AF[i] = (AI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
			}
			for(i=0; i<5; i++)
			{
				BF[i] = (BI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
			}
			for(i=0; i<5; i++)
			{
				CF[i] = (CI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
			}
			for(i=0; i<5; i++)
			{
				DF[i] = (DI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
			}
			for(i=0; i<5; i++)
			{
				EF[i] = (EI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
			}
			for(i=0; i<5; i++)
			{
				FF[i] = (FI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
			}
			for(i=0; i<5; i++)
			{
				GF[i] = (GI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
			}
			for(i=0; i<5; i++)
			{
				HF[i] = (HI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
			}
			
			/*Interpolation of shared scalars (at Interpolator)*/			
                        for(i=0; i<5; i++)
			{
				scalar_value_c[i] = (uint64_t) ( AF[i]*( (uint64_t)((tA+addMe)*multD) ) + BF[i]*((uint64_t) ((tB+addMe)*multD)) + CF[i]*((uint64_t) ((tC+addMe)*multD)) + DF[i]*((uint64_t) ((tD+addMe)*multD)) + EF[i]*((uint64_t) ((tE+addMe)*multD)) + FF[i]*((uint64_t) ((tF+addMe)*multD)) + GF[i]*((uint64_t) ((tG+addMe)*multD)) + HF[i]*((uint64_t) ((tH+addMe)*multD)) );
												
			}
      		
			//Recover secret scalar at compositor
			long int shr[3];
			uint64_t yshr[3], resh;			
			shr[0] = 1; shr[1] = 2; shr[2] = 3;
			for(i=0; i<3; i++)
				yshr[i] = scalar_value_c[i];
			resh=FindSecretFromShare64ModPC(shr, yshr, multQ);
			for(i=0; i<5; i++)
				scalar_value_c[i] = resh;			
						

      			if ( scalar_value < 0 ) 
        		{
        			scalar_value = 0;
        		}
      			else if (scalar_value > staticInfo->Volume->GetArraySize() - 1 )
        		{
        			scalar_value = (int)(staticInfo->Volume->GetArraySize() - 1);
        		}
			                        
			uint8_t tempI;
			uint64_t ttempI;
			opacity = SOTF[scalar_value];

			//Find opacity using recovered scalar value
			for(i=0; i<5; i++)
			{
				ttempI = scalar_value_c[i] / mult;
				tempI = ttempI / multD;			
				opacityS[i] = SOTF[tempI];  
			} 

			// If we have a combined opacity value, then compute the shading
	      		if( opacity )
	       		{
			 	if ( !grad_op_is_constant )
		    		{
		    			gmptr = grad_mag_ptr + offset;
		
					A = *(gmptr);
          				B = *(gmptr + Binc);
          				C = *(gmptr + Cinc);
          				D = *(gmptr + Dinc);
          				E = *(gmptr + Einc);
          				F = *(gmptr + Finc);
          				G = *(gmptr + Ginc);
          				H = *(gmptr + Hinc);

					gradient_value = (int) (A * tA + B * tB + C * tC + D * tD + E * tE + F * tF + G * tG + H * tH );
		    			if ( gradient_value < 0 )
		      			{
		      				gradient_value = 0;
		      			}
		    			else if ( gradient_value > 255 )
		      			{
		      				gradient_value = 255;
		      			}
					opacity *= GOTF[gradient_value];

					uint64_t a, b;
					a = rand() % multQ;
					b = rand() % multQ;	
			
					for(i=0; i<5; i++)
					{
						AF[i] = (AI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
					}
					for(i=0; i<5; i++)
					{
						BF[i] = (BI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
					}
					for(i=0; i<5; i++)
					{
						CF[i] = (CI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
					}
					for(i=0; i<5; i++)
					{
						DF[i] = (DI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
					}
					for(i=0; i<5; i++)
					{
						EF[i] = (EI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
					}
					for(i=0; i<5; i++)
					{
						FF[i] = (FI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
					}
					for(i=0; i<5; i++)
					{
						GF[i] = (GI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
					}
					for(i=0; i<5; i++)
					{
						HF[i] = (HI + (i+1)*a + (i+1)*(i+1)*b) % multQ;
					}					 
		    		}
		  		else
		    		{
		    			opacity *= gradient_opacity_constant;
		    		}
			}


			/*Interpolation of shared gradient (at Interpolator)*/			
                        for(i=0; i<5; i++)
			{
				gradient_value_c[i] = (uint64_t) ( AF[i]*( (uint64_t)((tA+addMe)*multD) ) + BF[i]*((uint64_t) ((tB+addMe)*multD)) + CF[i]*((uint64_t) ((tC+addMe)*multD)) + DF[i]*((uint64_t) ((tD+addMe)*multD)) + EF[i]*((uint64_t) ((tE+addMe)*multD)) + FF[i]*((uint64_t) ((tF+addMe)*multD)) + GF[i]*((uint64_t) ((tG+addMe)*multD)) + HF[i]*((uint64_t) ((tH+addMe)*multD)) );
												
			}


			//Recover secret gradient at compositor
			shr[0] = 1; shr[1] = 2; shr[2] = 3;
			for(i=0; i<3; i++)
				yshr[i] = gradient_value_c[i];
			resh=FindSecretFromShare64ModPC(shr, yshr, multQ);
			for(i=0; i<5; i++)
				gradient_value_c[i] = resh;	

			//Obtain the opacity according to gradients at the Compositor
			for(i=0; i<5; i++)
			{
				if( opacityS[i] )
	       			{
			 		if ( !grad_op_is_constant )
		    			{
		    				opacityS[i] *= gradient_value_c[i];				 
		    			}
			  		else
			    		{
			    			opacityS[i] *= gradient_opacity_constant;
			    		}
				}
			}				
			

			//Find the Phong Illumination factors at Interpolator
			if ( 1 )// We remove the optimization done by VTK as Interpolator does not know the secret opacities
			{
				
				A_n = *(nptr);
				B_n = *(nptr + Binc);
				C_n = *(nptr + Cinc);
				D_n = *(nptr + Dinc);
				E_n = *(nptr + Einc);
				F_n = *(nptr + Finc);
				G_n = *(nptr + Ginc);
				H_n = *(nptr + Hinc);
		
				final_rd = 
		  			red_d_shade[ A_n ] * tA + red_d_shade[ B_n ] * tB +   
		  			red_d_shade[ C_n ] * tC + red_d_shade[ D_n ] * tD + 
		  			red_d_shade[ E_n ] * tE + red_d_shade[ F_n ] * tF +   
		  			red_d_shade[ G_n ] * tG + red_d_shade[ H_n ] * tH;
		
				final_gd = 
		  			green_d_shade[ A_n ] * tA + green_d_shade[ B_n ] * tB +       
		  			green_d_shade[ C_n ] * tC + green_d_shade[ D_n ] * tD + 
		  			green_d_shade[ E_n ] * tE + green_d_shade[ F_n ] * tF +       
		  			green_d_shade[ G_n ] * tG + green_d_shade[ H_n ] * tH;
		
				final_bd = 
		  			blue_d_shade[ A_n ] * tA + blue_d_shade[ B_n ] * tB +         
		  			blue_d_shade[ C_n ] * tC + blue_d_shade[ D_n ] * tD + 
		  			blue_d_shade[ E_n ] * tE + blue_d_shade[ F_n ] * tF + 
		  			blue_d_shade[ G_n ] * tG + blue_d_shade[ H_n ] * tH;
		
				final_rs = 
		  			red_s_shade[ A_n ] * tA + red_s_shade[ B_n ] * tB +   
		  			red_s_shade[ C_n ] * tC + red_s_shade[ D_n ] * tD + 
		  			red_s_shade[ E_n ] * tE + red_s_shade[ F_n ] * tF +   
		  			red_s_shade[ G_n ] * tG + red_s_shade[ H_n ] * tH;
		
				final_gs = 
		  			green_s_shade[ A_n ] * tA + green_s_shade[ B_n ] * tB +       
		  			green_s_shade[ C_n ] * tC + green_s_shade[ D_n ] * tD + 
		  			green_s_shade[ E_n ] * tE + green_s_shade[ F_n ] * tF +       
		  			green_s_shade[ G_n ] * tG + green_s_shade[ H_n ] * tH;
		
				final_bs = 
		  			blue_s_shade[ A_n ] * tA + blue_s_shade[ B_n ] * tB +         
		  			blue_s_shade[ C_n ] * tC + blue_s_shade[ D_n ] * tD + 
		  			blue_s_shade[ E_n ] * tE + blue_s_shade[ F_n ] * tF + 
		  			blue_s_shade[ G_n ] * tG + blue_s_shade[ H_n ] * tH;				
				
			

				/*Interpolator creates shares of the shading factors*/

				//Scale
				rd_u[0] = (red_d_shade[ A_n ]+addMe) * mult;          rd_u[1] = (red_d_shade[ B_n ]+addMe) * mult; 
				rd_u[2] = (red_d_shade[ C_n ]+addMe) * mult;          rd_u[3] = (red_d_shade[ D_n ]+addMe) * mult;
				rd_u[4] = (red_d_shade[ E_n ]+addMe) * mult;          rd_u[5] = (red_d_shade[ F_n ]+addMe) * mult; 
				rd_u[6] = (red_d_shade[ G_n ]+addMe) * mult;          rd_u[7] = (red_d_shade[ H_n ]+addMe) * mult;

				//Secret Share
				aS = rand() % 25523; //just to decrease overhead
				bS = rand() % 25523;
				for(iS=0; iS<8; iS++)
				{
					for(xS=0; xS<5; xS++)
					{	
						rd_uS[xS][iS] = (rd_u[iS] + aS*(xS+1) + bS*(xS+1)*(xS+1)) % qS;
					}
				}
					
				//Scale			
				gd_u[0] = (green_d_shade[ A_n ]+addMe) * mult;          gd_u[1] = (green_d_shade[ B_n ]+addMe) * mult; 
				gd_u[2] = (green_d_shade[ C_n ]+addMe) * mult;          gd_u[3] = (green_d_shade[ D_n ]+addMe) * mult;
				gd_u[4] = (green_d_shade[ E_n ]+addMe) * mult;          gd_u[5] = (green_d_shade[ F_n ]+addMe) * mult; 
				gd_u[6] = (green_d_shade[ G_n ]+addMe) * mult;          gd_u[7] = (green_d_shade[ H_n ]+addMe) * mult;

				//Secret Share
				for(iS=0; iS<8; iS++)
				{
					for(xS=0; xS<5; xS++)
					{	
						gd_uS[xS][iS] = (gd_u[iS] + aS*(xS+1) + bS*(xS+1)*(xS+1)) % qS;
					}
				}	

				//Scale
				bd_u[0] = (blue_d_shade[ A_n ]+addMe) * mult;          bd_u[1] = (blue_d_shade[ B_n ]+addMe) * mult; 
				bd_u[2] = (blue_d_shade[ C_n ]+addMe) * mult;          bd_u[3] = (blue_d_shade[ D_n ]+addMe) * mult;
				bd_u[4] = (blue_d_shade[ E_n ]+addMe) * mult;          bd_u[5] = (blue_d_shade[ F_n ]+addMe) * mult; 
				bd_u[6] = (blue_d_shade[ G_n ]+addMe) * mult;          bd_u[7] = (blue_d_shade[ H_n ]+addMe) * mult;

				//Secret Share
				for(iS=0; iS<8; iS++)
				{
					for(xS=0; xS<5; xS++)
					{	
						bd_uS[xS][iS] = (bd_u[iS] + aS*(xS+1) + bS*(xS+1)*(xS+1)) % qS;
					}
				}
		
				//Scale	
				rs_u[0] = (red_s_shade[ A_n ]+addMe) * mult;          rs_u[1] = (red_s_shade[ B_n ]+addMe) * mult; 
				rs_u[2] = (red_s_shade[ C_n ]+addMe) * mult;          rs_u[3] = (red_s_shade[ D_n ]+addMe) * mult;
				rs_u[4] = (red_s_shade[ E_n ]+addMe) * mult;          rs_u[5] = (red_s_shade[ F_n ]+addMe) * mult; 
				rs_u[6] = (red_s_shade[ G_n ]+addMe) * mult;          rs_u[7] = (red_s_shade[ H_n ]+addMe) * mult;

				//Secret Share
				for(iS=0; iS<8; iS++)
				{
					for(xS=0; xS<5; xS++)
					{	
						rs_uS[xS][iS] = (rs_u[iS] + aS*(xS+1) + bS*(xS+1)*(xS+1)) % qS;
					}
				}	

				//Scale
				gs_u[0] = (green_s_shade[ A_n ]+addMe) * mult;          gs_u[1] = (green_s_shade[ B_n ]+addMe) * mult; 
				gs_u[2] = (green_s_shade[ C_n ]+addMe) * mult;          gs_u[3] = (green_s_shade[ D_n ]+addMe) * mult;
				gs_u[4] = (green_s_shade[ E_n ]+addMe) * mult;          gs_u[5] = (green_s_shade[ F_n ]+addMe) * mult; 
				gs_u[6] = (green_s_shade[ G_n ]+addMe) * mult;          gs_u[7] = (green_s_shade[ H_n ]+addMe) * mult;

				//Secret Share
				for(iS=0; iS<8; iS++)
				{
					for(xS=0; xS<5; xS++)
					{	
						gs_uS[xS][iS] = (gs_u[iS] + aS*(xS+1) + bS*(xS+1)*(xS+1)) % qS;
					}
				}	

				//Scale
				bs_u[0] = (blue_s_shade[ A_n ]+addMe) * mult;          bs_u[1] = (blue_s_shade[ B_n ]+addMe) * mult; 
				bs_u[2] = (blue_s_shade[ C_n ]+addMe) * mult;          bs_u[3] = (blue_s_shade[ D_n ]+addMe) * mult;
				bs_u[4] = (blue_s_shade[ E_n ]+addMe) * mult;          bs_u[5] = (blue_s_shade[ F_n ]+addMe) * mult; 
				bs_u[6] = (blue_s_shade[ G_n ]+addMe) * mult;          bs_u[7] = (blue_s_shade[ H_n ]+addMe) * mult;

				//Sceret Share
				for(iS=0; iS<8; iS++)
				{
					for(xS=0; xS<5; xS++)
					{	
						bs_uS[xS][iS] = (bs_u[iS] + aS*(xS+1) + bS*(xS+1)*(xS+1)) % qS;
					}
				}	

				//Trilinear Interpolation of Phong Illimuniation Factors at Compositor
				for(xS=0; xS<5; xS++)
				{
					frd_u[xS] = rd_uS[xS][0] * (uint64_t) ((tA+addMe)*multD) + rd_uS[xS][1] * (uint64_t) ((tB+addMe)*multD) + rd_uS[xS][2] * (uint64_t) ((tC+addMe)*multD) + rd_uS[xS][3] * (uint64_t) ((tD+addMe)*multD) + rd_uS[xS][4] * (uint64_t) ((tE+addMe)*multD) + rd_uS[xS][5] * (uint64_t) ((tF+addMe)*multD) + rd_uS[xS][6] * (uint64_t) ((tG+addMe)*multD) + rd_uS[xS][7] * (uint64_t) ((tH+addMe)*multD);

					fgd_u[xS] = gd_uS[xS][0] * (uint32_t) ((tA+addMe)*multD) + gd_uS[xS][1] * (uint32_t) ((tB+addMe)*multD) + gd_uS[xS][2] * (uint32_t) ((tC+addMe)*multD) + gd_uS[xS][3] * (uint32_t) ((tD+addMe)*multD) + gd_uS[xS][4] * (uint32_t) ((tE+addMe)*multD) + gd_uS[xS][5] * (uint32_t) ((tF+addMe)*multD) + gd_uS[xS][6] * (uint32_t) ((tG+addMe)*multD) + gd_uS[xS][7] * (uint32_t) ((tH+addMe)*multD);

					fbd_u[xS] = bd_uS[xS][0] * (uint32_t) ((tA+addMe)*multD) + bd_uS[xS][1] * (uint32_t) ((tB+addMe)*multD) + bd_uS[xS][2] * (uint32_t) ((tC+addMe)*multD) + bd_uS[xS][3] * (uint32_t) ((tD+addMe)*multD) + bd_uS[xS][4] * (uint32_t) ((tE+addMe)*multD) + bd_uS[xS][5] * (uint32_t) ((tF+addMe)*multD) + bd_uS[xS][6] * (uint32_t) ((tG+addMe)*multD) + bd_uS[xS][7] * (uint32_t) ((tH+addMe)*multD);

					frs_u[xS] = rs_uS[xS][0] * (uint32_t) ((tA+addMe)*multD) + rs_uS[xS][1] * (uint32_t) ((tB+addMe)*multD) + rs_uS[xS][2] * (uint32_t) ((tC+addMe)*multD) + rs_uS[xS][3] * (uint32_t) ((tD+addMe)*multD) + rs_uS[xS][4] * (uint32_t) ((tE+addMe)*multD) + rs_uS[xS][5] * (uint32_t) ((tF+addMe)*multD) + rs_uS[xS][6] * (uint32_t) ((tG+addMe)*multD) + rs_uS[xS][7] * (uint32_t) ((tH+addMe)*multD);

					fgs_u[xS] = gs_uS[xS][0] * (uint32_t) ((tA+addMe)*multD) + gs_uS[xS][1] * (uint32_t) ((tB+addMe)*multD) + gs_uS[xS][2] * (uint32_t) ((tC+addMe)*multD) + gs_uS[xS][3] * (uint32_t) ((tD+addMe)*multD) + gs_uS[xS][4] * (uint32_t) ((tE+addMe)*multD) + gs_uS[xS][5] * (uint32_t) ((tF+addMe)*multD) + gs_uS[xS][6] * (uint32_t) ((tG+addMe)*multD) + gs_uS[xS][7] * (uint32_t) ((tH+addMe)*multD);

					fbs_u[xS] = bs_uS[xS][0] * (uint32_t) ((tA+addMe)*multD) + bs_uS[xS][1] * (uint32_t) ((tB+addMe)*multD) + bs_uS[xS][2] * (uint32_t) ((tC+addMe)*multD) + bs_uS[xS][3] * (uint32_t) ((tD+addMe)*multD) + bs_uS[xS][4] * (uint32_t) ((tE+addMe)*multD) + bs_uS[xS][5] * (uint32_t) ((tF+addMe)*multD) + bs_uS[xS][6] * (uint32_t) ((tG+addMe)*multD) + bs_uS[xS][7] * (uint32_t) ((tH+addMe)*multD);
			}

		
			       //------------------------------------- Composition at the Compositor ---------------------------
							
				//Find the color

				/*for conventional rendering*/		
         			r = CTF[(scalar_value) * 3    ];
				g = CTF[(scalar_value) * 3 + 1];
				b = CTF[(scalar_value) * 3 + 2]; 

				/*Find color for secure rendering. Note: Look-up tables in each datacenters are the same*/
				for(iS=0; iS<5; iS++)
				{
					ttempI = scalar_value_c[iS] / mult; 
					tempI = ttempI / multD;	
					R1[iS] = (uint8_t) CTF_MyR1[iS][tempI];
					G1[iS] = (uint8_t) CTF_MyG1[iS][tempI];
					B1[iS] = (uint8_t) CTF_MyB1[iS][tempI];
				}
				 
				//Do the composition      
                                red_shaded_value   = opacity * ( final_rd * r + final_rs );
				green_shaded_value = opacity * ( final_gd * g + final_gs );
				blue_shaded_value  = opacity * ( final_bd * b + final_bs );
 
				//Composition of color shares
				for(iS=0; iS<5; iS++)
				{
					MyOiF1[iS] = remaining_opacityS[iS] * opacityS[iS];
					MyOint1 = MyOiF1[iS] * multS;

					TR[iS] = TR[iS] + R1[iS]*MyOint1*frd_u[iS] + MyOint1*frs_u[iS];
					TG[iS] = TG[iS] + G1[iS]*MyOint1*fgd_u[iS] + MyOint1*fgs_u[iS];
					TB[iS] = TB[iS] + B1[iS]*MyOint1*fbd_u[iS] + MyOint1*fbs_u[iS];
				}
				
				ctot++;				
		 		// Accumulate intensity and opacity for this sample location   
	       	 		accum_red_intensity   += red_shaded_value   * remaining_opacity;
				accum_green_intensity += green_shaded_value * remaining_opacity;
				accum_blue_intensity  += blue_shaded_value  * remaining_opacity;
				remaining_opacity *= (1.0 - opacity);
				for(iS=0; iS<5; iS++)
					remaining_opacityS[iS] *= (1.0 - opacityS[iS]);				
			}

	      		// Increment our position and compute our voxel location
	      		ray_position[0] += ray_increment[0];
	      		ray_position[1] += ray_increment[1];
	      		ray_position[2] += ray_increment[2];      
	     		voxel[0] = vtkFloorFuncMacro( ray_position[0] );
	      		voxel[1] = vtkFloorFuncMacro( ray_position[1] );
	      		voxel[2] = vtkFloorFuncMacro( ray_position[2] );	
		}
	}
   
	// Cap the accumulated intensity at 1.0
	if ( accum_red_intensity > 1.0 )
	{
	    	accum_red_intensity = 1.0;
	}
        if ( accum_green_intensity > 1.0 )
	{
		accum_green_intensity = 1.0;
	}
	if ( accum_blue_intensity > 1.0 )
	{
		accum_blue_intensity = 1.0;
	}
	  
	//Calculate original opacity
	if( remaining_opacity < VTK_REMAINING_OPACITY )
	{
		remaining_opacity = 0.0;
	}

	//Calculate shares of opacities
	for(iS=0; iS<5; iS++)
	{
		if( remaining_opacityS[iS] < VTK_REMAINING_OPACITY )
		{
			remaining_opacityS[iS] = 0.0;
		}
	}

	//Return the rendered colors

	//share
	for(l=0; l<5; l++)
  	{
		dynamicInfo->ColorShare1[l][0] = TR[l];
		dynamicInfo->ColorShare1[l][1] = TG[l];
		dynamicInfo->ColorShare1[l][2] = TB[l];
		dynamicInfo->ColorShare1[l][3] = (1.0 - remaining_opacityS[l]) * 1000000;				
 	}

	//original
	dynamicInfo->Color[0] = accum_red_intensity * 255;
  	dynamicInfo->Color[1] = accum_green_intensity * 255;
  	dynamicInfo->Color[2] = accum_blue_intensity * 255;
  	dynamicInfo->Color[3] = 1.0 - remaining_opacity;
  	dynamicInfo->NumberOfStepsTaken = steps_this_ray;	
 }

// ============================================================ END HERE ===============================================================
 


// Description:
// This is the templated function that actually casts a ray and computes
// the composite value.  This version uses trilinear interpolation and
// does not compute shading
template <class T>
void vtkCastRay_TrilinVertices_Unshaded( T *data_ptr, vtkVolumeRayCastDynamicInfo *dynamicInfo, vtkVolumeRayCastStaticInfo *staticInfo )
{
	unsigned char   *grad_mag_ptr = NULL;
  	unsigned char   *goptr;
  	float           accum_red_intensity;
  	float           accum_green_intensity;
  	float           accum_blue_intensity;
  	float           remaining_opacity;
    	float           red_value, green_value, blue_value;
  	float           opacity;
  	int             loop;
  	int             xinc, yinc, zinc;
  	int             voxel[3];
  	float           ray_position[3];
  	int             prev_voxel[3];
  	float           A, B, C, D, E, F, G, H;
  	float           Ago, Bgo, Cgo, Dgo, Ego, Fgo, Ggo, Hgo;
  	int             Binc, Cinc, Dinc, Einc, Finc, Ginc, Hinc;
  	T               *dptr;
  	float           *SOTF;
  	float           *CTF;
  	float           *GTF;
  	float           *GOTF;
  	float           x, y, z, t1, t2, t3;
  	float           weight;
  	int             offset, i;
  	int             steps_this_ray = 0;
  	int             num_steps;
  	float           *ray_start, *ray_increment;
  	int             grad_op_is_constant;
  	float           gradient_opacity_constant;
	float  opacityS[5], remaining_opacityS[5];

  	num_steps = dynamicInfo->NumberOfStepsToTake;
  	ray_start = dynamicInfo->TransformedStart;
  	ray_increment = dynamicInfo->TransformedIncrement;
 
  	// Get the scalar opacity transfer function which maps scalar input values
  	// to opacities
  	SOTF =  staticInfo->Volume->GetCorrectedScalarOpacityArray();

  	// Get the color transfer function which maps scalar input values
  	// to RGB colors
  	CTF =  staticInfo->Volume->GetRGBArray();
  	GTF =  staticInfo->Volume->GetGrayArray();

  	// Get the gradient opacity transfer function for this volume (which maps
  	// gradient magnitudes to opacities)
  	GOTF =  staticInfo->Volume->GetGradientOpacityArray();

  	// Get the gradient opacity constant. If this number is greater than
  	// or equal to 0.0, then the gradient opacity transfer function is
  	// a constant at that value, otherwise it is not a constant function
  	gradient_opacity_constant = staticInfo->Volume->GetGradientOpacityConstant();
  	grad_op_is_constant = ( gradient_opacity_constant >= 0.0 );

  	// Get a pointer to the gradient magnitudes for this volume
  	if ( !grad_op_is_constant )
    	{
   		grad_mag_ptr = staticInfo->GradientMagnitudes;
    	}

  	// Move the increments into local variables
  	xinc = staticInfo->DataIncrement[0];
  	yinc = staticInfo->DataIncrement[1];
  	zinc = staticInfo->DataIncrement[2];

  	// Initialize the ray position and voxel location
  	ray_position[0] = ray_start[0];
  	ray_position[1] = ray_start[1];
  	ray_position[2] = ray_start[2];
  	voxel[0] = vtkFloorFuncMacro( ray_position[0] );
  	voxel[1] = vtkFloorFuncMacro( ray_position[1] );
  	voxel[2] = vtkFloorFuncMacro( ray_position[2] );

  	// So far we have not accumulated anything
  	accum_red_intensity     = 0.0;
  	accum_green_intensity   = 0.0;
  	accum_blue_intensity    = 0.0;
  	remaining_opacity       = 1.0;  
  	for(i=0; i<5; i++)
		remaining_opacityS[i] = 1.0;

  	// Compute the increments to get to the other 7 voxel vertices from A
  	Binc = xinc;
  	Cinc = yinc;
  	Dinc = xinc + yinc;
  	Einc = zinc;
  	Finc = zinc + xinc;
  	Ginc = zinc + yinc;
  	Hinc = zinc + xinc + yinc;
  
  	// Compute the values for the first pass through the loop
  	offset = voxel[2] * zinc + voxel[1] * yinc + voxel[0];
  	dptr   = data_ptr + offset;

  	A = SOTF[(*(dptr))];
 	 B = SOTF[(*(dptr + Binc))];
  	C = SOTF[(*(dptr + Cinc))];
  	D = SOTF[(*(dptr + Dinc))];
  	E = SOTF[(*(dptr + Einc))];
  	F = SOTF[(*(dptr + Finc))];
  	G = SOTF[(*(dptr + Ginc))];
  	H = SOTF[(*(dptr + Hinc))];  

  	if ( !grad_op_is_constant )
    	{
    		goptr  = grad_mag_ptr + offset;
    		Ago = GOTF[(*(goptr))];
    		Bgo = GOTF[(*(goptr + Binc))];
    		Cgo = GOTF[(*(goptr + Cinc))];
    		Dgo = GOTF[(*(goptr + Dinc))];
    		Ego = GOTF[(*(goptr + Einc))];
    		Fgo = GOTF[(*(goptr + Finc))];
    		Ggo = GOTF[(*(goptr + Ginc))];
    		Hgo = GOTF[(*(goptr + Hinc))];  
    	}
  	else 
    	{
    		Ago = Bgo = Cgo = Dgo = Ego = Fgo = Ggo = Hgo = 1.0;
    	}

  	// Keep track of previous voxel to know when we step into a new one  
  	prev_voxel[0] = voxel[0];
  	prev_voxel[1] = voxel[1];
  	prev_voxel[2] = voxel[2];

  	// Two cases - we are working with a gray or RGB transfer
  	// function - break them up to make it more efficient
  	if ( staticInfo->ColorChannels == 1 ) 
    	{
    		// For each step along the ray
    		for ( loop = 0; loop < num_steps && remaining_opacity > VTK_REMAINING_OPACITY; loop++ )
      		{     
      			// We've taken another step
      			steps_this_ray++;
      
      			// Have we moved into a new voxel? If so we need to recompute A-H
      			if ( prev_voxel[0] != voxel[0] || prev_voxel[1] != voxel[1] || prev_voxel[2] != voxel[2] )
        		{
        			offset = voxel[2] * zinc + voxel[1] * yinc + voxel[0];
        			dptr   = data_ptr + offset;
        
        			A = SOTF[(*(dptr))];
        			B = SOTF[(*(dptr + Binc))];
        			C = SOTF[(*(dptr + Cinc))];
        			D = SOTF[(*(dptr + Dinc))];
        			E = SOTF[(*(dptr + Einc))];
        			F = SOTF[(*(dptr + Finc))];
        			G = SOTF[(*(dptr + Ginc))];
        			H = SOTF[(*(dptr + Hinc))];

        			if ( !grad_op_is_constant )
          			{
          				goptr  = grad_mag_ptr + offset;
          				Ago = GOTF[(*(goptr))];
          				Bgo = GOTF[(*(goptr + Binc))];
					  Cgo = GOTF[(*(goptr + Cinc))];
					  Dgo = GOTF[(*(goptr + Dinc))];
					  Ego = GOTF[(*(goptr + Einc))];
					  Fgo = GOTF[(*(goptr + Finc))];
					  Ggo = GOTF[(*(goptr + Ginc))];
					  Hgo = GOTF[(*(goptr + Hinc))];  
          			}
        			else 
          			{
            				Ago = Bgo = Cgo = Dgo = Ego = Fgo = Ggo = Hgo = 1.0;
          			}

				prev_voxel[0] = voxel[0];
				prev_voxel[1] = voxel[1];
				prev_voxel[2] = voxel[2];
        		}
      
		      // Compute our offset in the voxel, and use that to trilinearly
		      // interpolate the value
		      x = ray_position[0] - (float) voxel[0];
		      y = ray_position[1] - (float) voxel[1];
		      z = ray_position[2] - (float) voxel[2];
      
		      t1 = 1.0 - x;
		      t2 = 1.0 - y;
		      t3 = 1.0 - z;
      
		      // For this sample we have do not yet have any opacity
		      opacity          = 0.0;
		      red_value        = 0.0;
		      for(i=0; i<5; i++)
			opacityS[i] = 0;	
      
      // Now add the opacity in vertex by vertex.  If any of the A-H
      // have a non-transparent opacity value, then add its contribution
      // to the opacity
      if ( A && Ago )
        {
        weight     = t1*t2*t3 * A * Ago;
        opacity   += weight;
        red_value += weight * GTF[((*dptr))];
        }
      
      if ( B && Bgo )
        {
        weight     = x*t2*t3 * B * Bgo;
        opacity   += weight;
        red_value += weight * GTF[((*(dptr + Binc)))];
        }
      
      if ( C && Cgo )
        {
        weight     = t1*y*t3 * C * Cgo;
        opacity   += weight;
        red_value += weight * GTF[((*(dptr + Cinc)))];
        }
      
      if ( D && Dgo )
        {
        weight     = x*y*t3 * D * Dgo;
        opacity   += weight;
        red_value += weight * GTF[((*(dptr + Dinc)))];
        }
      
      if ( E && Ego )
        {
        weight     = t1*t2*z * E * Ego; 
        opacity   += weight;
        red_value += weight * GTF[((*(dptr + Einc)))];
        }
      
      if ( F && Fgo )
        {
        weight     = x*t2*z * F * Fgo;
        opacity   += weight;
        red_value += weight * GTF[((*(dptr + Finc)))];
        }
      
      if ( G && Ggo )
        {
        weight     = t1*y*z * G * Ggo;
        opacity   += weight;
        red_value += weight * GTF[((*(dptr + Ginc)))];
        } 
      
      if ( H && Hgo )
        {
        weight     = x*z*y * H * Hgo;
        opacity   += weight;
        red_value += weight * GTF[((*(dptr + Hinc)))];
        }
      
      // Accumulate intensity and opacity for this sample location
      accum_red_intensity   += remaining_opacity * red_value;
      remaining_opacity *= (1.0 - opacity);
      
      // Increment our position and compute our voxel location
      ray_position[0] += ray_increment[0];
      ray_position[1] += ray_increment[1];
      ray_position[2] += ray_increment[2];      
      voxel[0] = vtkFloorFuncMacro( ray_position[0] );
      voxel[1] = vtkFloorFuncMacro( ray_position[1] );
      voxel[2] = vtkFloorFuncMacro( ray_position[2] );
      }
    accum_green_intensity = accum_red_intensity;
    accum_blue_intensity = accum_red_intensity;
    }
  else if ( staticInfo->ColorChannels == 3 )
    {
    // For each step along the ray
    for ( loop = 0; 
          loop < num_steps && remaining_opacity > VTK_REMAINING_OPACITY; 
          loop++ )
      {     
      // We've taken another step
      steps_this_ray++;
      
      // Have we moved into a new voxel? If so we need to recompute A-H
      if ( prev_voxel[0] != voxel[0] ||
           prev_voxel[1] != voxel[1] ||
           prev_voxel[2] != voxel[2] )
        {
        offset = voxel[2] * zinc + voxel[1] * yinc + voxel[0];
        dptr = data_ptr + offset;
        
        A = SOTF[(*(dptr))];
        B = SOTF[(*(dptr + Binc))];
        C = SOTF[(*(dptr + Cinc))];
        D = SOTF[(*(dptr + Dinc))];
        E = SOTF[(*(dptr + Einc))];
        F = SOTF[(*(dptr + Finc))];
        G = SOTF[(*(dptr + Ginc))];
        H = SOTF[(*(dptr + Hinc))];

        if ( !grad_op_is_constant ) 
          {
          goptr  = grad_mag_ptr + offset;
          Ago = GOTF[(*(goptr))];
          Bgo = GOTF[(*(goptr + Binc))];
          Cgo = GOTF[(*(goptr + Cinc))];
          Dgo = GOTF[(*(goptr + Dinc))];
          Ego = GOTF[(*(goptr + Einc))];
          Fgo = GOTF[(*(goptr + Finc))];
          Ggo = GOTF[(*(goptr + Ginc))];
          Hgo = GOTF[(*(goptr + Hinc))];  
          }
        else 
          {
            Ago = Bgo = Cgo = Dgo = Ego = Fgo = Ggo = Hgo = 1.0;
          }

        prev_voxel[0] = voxel[0];
        prev_voxel[1] = voxel[1];
        prev_voxel[2] = voxel[2];
        }
      
      // Compute our offset in the voxel, and use that to trilinearly
      // interpolate the value
      x = ray_position[0] - (float) voxel[0];
      y = ray_position[1] - (float) voxel[1];
      z = ray_position[2] - (float) voxel[2];
      
      t1 = 1.0 - x;
      t2 = 1.0 - y;
      t3 = 1.0 - z;
      
      // For this sample we have do not yet have any opacity
      opacity           = 0.0;
      red_value         = 0.0;
      green_value       = 0.0;
      blue_value        = 0.0;
      
      // Now add the opacity in vertex by vertex.  If any of the A-H
      // have a non-transparent opacity value, then add its contribution
      // to the opacity
      if ( A && Ago )
        {
        weight         = t1*t2*t3 * A * Ago;
        opacity       += weight;
        red_value     += weight * CTF[((*dptr)) * 3    ];
        green_value   += weight * CTF[((*dptr)) * 3 + 1];
        blue_value    += weight * CTF[((*dptr)) * 3 + 2];
        }
      
      if ( B && Bgo )
        {
        weight         = x*t2*t3 * B * Bgo;
        opacity       += weight;
        red_value     += weight * CTF[((*(dptr + Binc))) * 3    ];
        green_value   += weight * CTF[((*(dptr + Binc))) * 3 + 1];
        blue_value    += weight * CTF[((*(dptr + Binc))) * 3 + 2];
        }
      
      if ( C && Cgo )
        {
        weight         = t1*y*t3 * C * Cgo;
        opacity       += weight;
        red_value     += weight * CTF[((*(dptr + Cinc))) * 3    ];
        green_value   += weight * CTF[((*(dptr + Cinc))) * 3 + 1];
        blue_value    += weight * CTF[((*(dptr + Cinc))) * 3 + 2];
        }
      
      if ( D && Dgo )
        {
        weight         = x*y*t3 * D * Dgo;
        opacity       += weight;
        red_value     += weight * CTF[((*(dptr + Dinc))) * 3    ];
        green_value   += weight * CTF[((*(dptr + Dinc))) * 3 + 1];
        blue_value    += weight * CTF[((*(dptr + Dinc))) * 3 + 2];
        }
      
      if ( E && Ego )
        {
        weight         = t1*t2*z * E * Ego;
        opacity       += weight;
        red_value     += weight * CTF[((*(dptr + Einc))) * 3    ];
        green_value   += weight * CTF[((*(dptr + Einc))) * 3 + 1];
        blue_value    += weight * CTF[((*(dptr + Einc))) * 3 + 2];
        }
      
      if ( F && Fgo )
        {
        weight         = x*t2*z * F * Fgo;
        opacity       += weight;
        red_value     += weight * CTF[((*(dptr + Finc))) * 3    ];
        green_value   += weight * CTF[((*(dptr + Finc))) * 3 + 1];
        blue_value    += weight * CTF[((*(dptr + Finc))) * 3 + 2];
        }
      
      if ( G && Ggo )
        {
        weight         = t1*y*z * G * Ggo;
        opacity       += weight;
        red_value     +=  weight * CTF[((*(dptr + Ginc))) * 3    ];
        green_value   +=  weight * CTF[((*(dptr + Ginc))) * 3 + 1];
        blue_value    +=  weight * CTF[((*(dptr + Ginc))) * 3 + 2];
        } 
      
      if ( H && Hgo )
        {
        weight         = x*z*y * H * Hgo;
        opacity       += weight;
        red_value     += weight * CTF[((*(dptr + Hinc))) * 3    ];
        green_value   += weight * CTF[((*(dptr + Hinc))) * 3 + 1];
        blue_value    += weight * CTF[((*(dptr + Hinc))) * 3 + 2];
        }
      
      // Accumulate intensity and opacity for this sample location
      accum_red_intensity   += remaining_opacity * red_value;
      accum_green_intensity += remaining_opacity * green_value;
      accum_blue_intensity  += remaining_opacity * blue_value;
      remaining_opacity *= (1.0 - opacity);
      
      // Increment our position and compute our voxel location
      ray_position[0] += ray_increment[0];
      ray_position[1] += ray_increment[1];
      ray_position[2] += ray_increment[2];      
      voxel[0] = vtkFloorFuncMacro( ray_position[0] );
      voxel[1] = vtkFloorFuncMacro( ray_position[1] );
      voxel[2] = vtkFloorFuncMacro( ray_position[2] );
      }
    }

  // Cap the accumulated intensity at 1.0
  if ( accum_red_intensity > 1.0 )
    {
    accum_red_intensity = 1.0;
    }
  if ( accum_green_intensity > 1.0 )
    {
    accum_green_intensity = 1.0;
    }
  if ( accum_blue_intensity > 1.0 )
    {
    accum_blue_intensity = 1.0;
    }
  
  if( remaining_opacity < VTK_REMAINING_OPACITY )
    {
    remaining_opacity = 0.0;
    }

  // Set the return pixel value.  The depth value is the distance to the
  // center of the volume.
  dynamicInfo->Color[0] = accum_red_intensity;
  dynamicInfo->Color[1] = accum_green_intensity;
  dynamicInfo->Color[2] = accum_blue_intensity;
  dynamicInfo->Color[3] = 1.0 - remaining_opacity;
  dynamicInfo->NumberOfStepsTaken = steps_this_ray;


}


// Description:
// This is the templated function that actually casts a ray and computes
// the composite value.  This version uses trilinear interpolation, and
// does perform shading.
template <class T>
void vtkCastRay_TrilinVertices_Shaded( T *data_ptr, vtkVolumeRayCastDynamicInfo *dynamicInfo,
                                       vtkVolumeRayCastStaticInfo *staticInfo )
{
  unsigned char   *grad_mag_ptr = NULL;
  unsigned char   *goptr;
  float           accum_red_intensity;
  float           accum_green_intensity;
  float           accum_blue_intensity;
  float		  accm_red_Share;
  float		  accm_green_Share;
  float		  accm_blue_Share;
  float           remaining_opacity;
  float           opacity;
  int             loop;
  int             xinc, yinc, zinc;
  int             voxel[3];
  float           ray_position[3];
  int             prev_voxel[3];
  float           A, B, C, D, E, F, G, H;
  float           Ago, Bgo, Cgo, Dgo, Ego, Fgo, Ggo, Hgo;
  int             Binc, Cinc, Dinc, Einc, Finc, Ginc, Hinc;
  T               *dptr;
  float           *SOTF;
  float           *CTF;
  float           *GTF;
  float           *GOTF;
  float           x, y, z, t1, t2, t3;
  float           weight;
  float           *red_d_shade, *green_d_shade, *blue_d_shade;
  float           *red_s_shade, *green_s_shade, *blue_s_shade;
  unsigned short  *encoded_normals, *nptr;
  float           red_shaded_value, green_shaded_value, blue_shaded_value;
  float		  red_shaded_Share[5], green_shaded_Share[5], blue_shaded_Share[5];
  int             offset;
  int             steps_this_ray = 0;
  int             grad_op_is_constant;
  float           gradient_opacity_constant;
  int             num_steps;
  float           *ray_start, *ray_increment;
  int l;

  num_steps = dynamicInfo->NumberOfStepsToTake;
  ray_start = dynamicInfo->TransformedStart;
  ray_increment = dynamicInfo->TransformedIncrement;

  // Get diffuse shading table pointers
  red_d_shade = staticInfo->RedDiffuseShadingTable;
  green_d_shade = staticInfo->GreenDiffuseShadingTable;
  blue_d_shade = staticInfo->BlueDiffuseShadingTable;


  // Get diffuse shading table pointers
  red_s_shade = staticInfo->RedSpecularShadingTable;
  green_s_shade = staticInfo->GreenSpecularShadingTable;
  blue_s_shade = staticInfo->BlueSpecularShadingTable;

  // Get a pointer to the encoded normals for this volume
  encoded_normals = staticInfo->EncodedNormals;

  // Get the scalar opacity transfer function which maps scalar input values
  // to opacities
  SOTF =  staticInfo->Volume->GetCorrectedScalarOpacityArray();

  // Get the color transfer function which maps scalar input values
  // to RGB values
  CTF =  staticInfo->Volume->GetRGBArray();
  GTF =  staticInfo->Volume->GetGrayArray();

  // Get the gradient opacity transfer function for this volume (which maps
  // gradient magnitudes to opacities)
  GOTF =  staticInfo->Volume->GetGradientOpacityArray();

  // Get the gradient opacity constant. If this number is greater than
  // or equal to 0.0, then the gradient opacity transfer function is
  // a constant at that value, otherwise it is not a constant function
  gradient_opacity_constant = staticInfo->Volume->GetGradientOpacityConstant();
  grad_op_is_constant = ( gradient_opacity_constant >= 0.0 );

  // Get a pointer to the gradient magnitudes for this volume
  if ( !grad_op_is_constant )
    {
    grad_mag_ptr = staticInfo->GradientMagnitudes;
    }

  // Move the increments into local variables
  xinc = staticInfo->DataIncrement[0];
  yinc = staticInfo->DataIncrement[1];
  zinc = staticInfo->DataIncrement[2];

  // Initialize the ray position and voxel location
  ray_position[0] = ray_start[0];
  ray_position[1] = ray_start[1];
  ray_position[2] = ray_start[2];
  voxel[0] = vtkFloorFuncMacro( ray_position[0] );
  voxel[1] = vtkFloorFuncMacro( ray_position[1] );
  voxel[2] = vtkFloorFuncMacro( ray_position[2] );

  // So far we haven't accumulated anything
  accum_red_intensity     = 0.0;
  accum_green_intensity   = 0.0;
  accum_blue_intensity    = 0.0;
  remaining_opacity       = 1.0;

  // Compute the increments to get to the other 7 voxel vertices from A
  Binc = xinc;
  Cinc = yinc;
  Dinc = xinc + yinc;
  Einc = zinc;
  Finc = zinc + xinc;
  Ginc = zinc + yinc;
  Hinc = zinc + xinc + yinc;
  
   // Compute the values for the first pass through the loop
  offset = voxel[2] * zinc + voxel[1] * yinc + voxel[0];
  dptr   = data_ptr + offset;
  nptr   = encoded_normals + offset;

  A = SOTF[(*(dptr))];
  B = SOTF[(*(dptr + Binc))];
  C = SOTF[(*(dptr + Cinc))];
  D = SOTF[(*(dptr + Dinc))];
  E = SOTF[(*(dptr + Einc))];
  F = SOTF[(*(dptr + Finc))];
  G = SOTF[(*(dptr + Ginc))];
  H = SOTF[(*(dptr + Hinc))];

  if ( !grad_op_is_constant )
    {
    goptr  = grad_mag_ptr + offset;
    Ago = GOTF[(*(goptr))];
    Bgo = GOTF[(*(goptr + Binc))];
    Cgo = GOTF[(*(goptr + Cinc))];
    Dgo = GOTF[(*(goptr + Dinc))];
    Ego = GOTF[(*(goptr + Einc))];
    Fgo = GOTF[(*(goptr + Finc))];
    Ggo = GOTF[(*(goptr + Ginc))];
    Hgo = GOTF[(*(goptr + Hinc))];  
    }
  else
    {
    Ago = Bgo = Cgo = Dgo = Ego = Fgo = Ggo = Hgo = 1.0;
    }


  // Keep track of previous voxel to know when we step into a new one   
  prev_voxel[0] = voxel[0];
  prev_voxel[1] = voxel[1];
  prev_voxel[2] = voxel[2];

  // Two cases - we are working with a single color or a color transfer
  // function - break them up to make it more efficient
  if ( staticInfo->ColorChannels == 1 )
    {
    // For each step along the ray
    for ( loop = 0; 
          loop < num_steps && remaining_opacity > VTK_REMAINING_OPACITY; 
          loop++ )
      {     
      // We've taken another step
      steps_this_ray++;
      
      // Have we moved into a new voxel? If so we need to recompute A-H
      if ( prev_voxel[0] != voxel[0] ||
           prev_voxel[1] != voxel[1] ||
           prev_voxel[2] != voxel[2] )
        {
        offset = voxel[2] * zinc + voxel[1] * yinc + voxel[0];
        dptr   = data_ptr + offset;
        nptr   = encoded_normals + offset;
        //goptr  = grad_mag_ptr + offset;
        
        A = SOTF[(*(dptr))];
        B = SOTF[(*(dptr + Binc))];
        C = SOTF[(*(dptr + Cinc))];
        D = SOTF[(*(dptr + Dinc))];
        E = SOTF[(*(dptr + Einc))];
        F = SOTF[(*(dptr + Finc))];
        G = SOTF[(*(dptr + Ginc))];
        H = SOTF[(*(dptr + Hinc))];
        
        if ( !grad_op_is_constant )
          {
          goptr  = grad_mag_ptr + offset;
          Ago = GOTF[(*(goptr))];
          Bgo = GOTF[(*(goptr + Binc))];
          Cgo = GOTF[(*(goptr + Cinc))];
          Dgo = GOTF[(*(goptr + Dinc))];
          Ego = GOTF[(*(goptr + Einc))];
          Fgo = GOTF[(*(goptr + Finc))];
          Ggo = GOTF[(*(goptr + Ginc))];
          Hgo = GOTF[(*(goptr + Hinc))];  
          }
        else
          {
          Ago = Bgo = Cgo = Dgo = Ego = Fgo = Ggo = Hgo = 1.0;
          }

        prev_voxel[0] = voxel[0];
        prev_voxel[1] = voxel[1];
        prev_voxel[2] = voxel[2];
        }
      
      // Compute our offset in the voxel, and use that to trilinearly
      // interpolate a value
      x = ray_position[0] - (float) voxel[0];
      y = ray_position[1] - (float) voxel[1];
      z = ray_position[2] - (float) voxel[2];
      
      t1 = 1.0 - x;
      t2 = 1.0 - y;
      t3 = 1.0 - z;
      
      // For this sample we have do not yet have any opacity or
      // shaded intensity yet
      opacity = 0.0;
      red_shaded_value = 0.0;
      
      // Now add the opacity and shaded intensity value in vertex by 
      // vertex.  If any of the A-H have a non-transparent opacity value, 
      // then add its contribution to the opacity
      if ( A && Ago )
        {
        weight = t1*t2*t3 * A * Ago;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr) ] * 
                                     GTF[*(dptr)] + 
                                     red_s_shade[ *(nptr) ] );
        }
      
      if ( B && Bgo )
        {
        weight = x*t2*t3 * B * Bgo;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr + Binc) ] * 
                                     GTF[*(dptr+Binc)] + 
                                     red_s_shade[ *(nptr + Binc) ] );
        }
      
      if ( C && Cgo )
        {
        weight = t1*y*t3 * C * Cgo;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr + Cinc) ] *
                                     GTF[*(dptr+Cinc)] + 
                                     red_s_shade[ *(nptr + Cinc) ] );
        }

      if ( D && Dgo )
        {
        weight = x*y*t3 * D * Dgo;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr + Dinc) ] *
                                     GTF[*(dptr+Dinc)] + 
                                     red_s_shade[ *(nptr + Dinc) ] );
        }
      
      if ( E && Ego )
        {
        weight = t1*t2*z * E * Ego;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr + Einc) ] *
                                     GTF[*(dptr+Einc)] + 
                                     red_s_shade[ *(nptr + Einc) ] );
        }
      
      if ( F && Fgo )
        {
        weight = x*z*t2 * F * Fgo;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr + Finc) ] *
                                     GTF[*(dptr+Finc)] + 
                                     red_s_shade[ *(nptr + Finc) ] );
        }
      
      if ( G && Ggo )
        {
        weight = t1*y*z * G * Ggo;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr + Ginc) ] *
                                     GTF[*(dptr+Ginc)] + 
                                     red_s_shade[ *(nptr + Ginc) ] );
      } 
      
      if ( H && Hgo )
        {
        weight = x*z*y * H * Hgo;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr + Hinc) ] *
                                     GTF[*(dptr+Hinc)] + 
                                     red_s_shade[ *(nptr + Hinc) ] );
        }
      
      // Accumulate intensity and opacity for this sample location   
      accum_red_intensity   += red_shaded_value   * remaining_opacity;
      remaining_opacity *= (1.0 - opacity);
      
      // Increment our position and compute our voxel location
      ray_position[0] += ray_increment[0];
      ray_position[1] += ray_increment[1];
      ray_position[2] += ray_increment[2];      
      voxel[0] = vtkFloorFuncMacro( ray_position[0] );
      voxel[1] = vtkFloorFuncMacro( ray_position[1] );
      voxel[2] = vtkFloorFuncMacro( ray_position[2] );
      }
    accum_green_intensity = accum_red_intensity;
    accum_blue_intensity = accum_red_intensity;
    }
  else if ( staticInfo->ColorChannels == 3 )
    {
    // For each step along the ray
    for ( loop = 0; 
          loop < num_steps && remaining_opacity > VTK_REMAINING_OPACITY; 
          loop++ )
      {     
      // We've taken another step
      steps_this_ray++;
      
      // Have we moved into a new voxel? If so we need to recompute A-H
      if ( prev_voxel[0] != voxel[0] ||
           prev_voxel[1] != voxel[1] ||
           prev_voxel[2] != voxel[2] )
        {
        offset = voxel[2] * zinc + voxel[1] * yinc + voxel[0];
        dptr   = data_ptr + offset;
        nptr   = encoded_normals + offset;
        
        A = SOTF[(*(dptr))];
        B = SOTF[(*(dptr + Binc))];
        C = SOTF[(*(dptr + Cinc))];
        D = SOTF[(*(dptr + Dinc))];
        E = SOTF[(*(dptr + Einc))];
        F = SOTF[(*(dptr + Finc))];
        G = SOTF[(*(dptr + Ginc))];
        H = SOTF[(*(dptr + Hinc))];
        
        if ( !grad_op_is_constant )
          {
          goptr  = grad_mag_ptr + offset;
          Ago = GOTF[(*(goptr))];
          Bgo = GOTF[(*(goptr + Binc))];
          Cgo = GOTF[(*(goptr + Cinc))];
          Dgo = GOTF[(*(goptr + Dinc))];
          Ego = GOTF[(*(goptr + Einc))];
          Fgo = GOTF[(*(goptr + Finc))];
          Ggo = GOTF[(*(goptr + Ginc))];
          Hgo = GOTF[(*(goptr + Hinc))];  
          }
        else
          {
          Ago = Bgo = Cgo = Dgo = Ego = Fgo = Ggo = Hgo = 1.0;
          }

        prev_voxel[0] = voxel[0];
        prev_voxel[1] = voxel[1];
        prev_voxel[2] = voxel[2];
        }
      
      // Compute our offset in the voxel, and use that to trilinearly
      // interpolate a value
      x = ray_position[0] - (float) voxel[0];
      y = ray_position[1] - (float) voxel[1];
      z = ray_position[2] - (float) voxel[2];
      
      t1 = 1.0 - x;
      t2 = 1.0 - y;
      t3 = 1.0 - z;
      
      // For this sample we have do not yet have any opacity or
      // shaded intensity yet
      opacity = 0.0;
      red_shaded_value = 0.0;
      green_shaded_value = 0.0;
      blue_shaded_value = 0.0;
      
      // Now add the opacity and shaded intensity value in vertex by 
      // vertex.  If any of the A-H have a non-transparent opacity value, 
      // then add its contribution to the opacity
      if ( A )
        {
        weight = t1*t2*t3 * A * Ago;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr) ] * 
                                     CTF[*(dptr) * 3] + 
                                     red_s_shade[ *(nptr) ] );
        green_shaded_value += weight * ( green_d_shade[ *(nptr) ] * 
                                     CTF[*(dptr) * 3 + 1] + + 
                                     green_s_shade[ *(nptr) ] );
        blue_shaded_value  += weight * ( blue_d_shade[ *(nptr) ] * 
                                     CTF[*(dptr) * 3 + 2] +
                                     blue_s_shade[ *(nptr) ]  );
        }
      
      if ( B )
        {
        weight = x*t2*t3 * B * Bgo;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr + Binc) ] * 
                                     CTF[*(dptr+Binc) * 3] + 
                                     red_s_shade[ *(nptr + Binc) ] );
        green_shaded_value += weight * ( green_d_shade[ *(nptr + Binc) ] *
                                     CTF[*(dptr+Binc) * 3 + 1] + 
                                     green_s_shade[ *(nptr + Binc) ] );
        blue_shaded_value  += weight * ( blue_d_shade[ *(nptr + Binc) ] *
                                     CTF[*(dptr+Binc) * 3 + 2] +
                                     blue_s_shade[ *(nptr + Binc) ] );
        }
      
      if ( C )
        {
        weight = t1*y*t3 * C * Cgo;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr + Cinc) ] *
                                     CTF[*(dptr+Cinc) * 3] + 
                                     red_s_shade[ *(nptr + Cinc) ] );
        green_shaded_value += weight * ( green_d_shade[ *(nptr + Cinc) ] *
                                     CTF[*(dptr+Cinc) * 3 + 1] + 
                                     green_s_shade[ *(nptr + Cinc) ] );
        blue_shaded_value  += weight * ( blue_d_shade[ *(nptr + Cinc) ] *
                                     CTF[*(dptr+Cinc) * 3 + 2] +
                                     blue_s_shade[ *(nptr + Cinc) ] );
        }

      if ( D )
        {
        weight = x*y*t3 * D * Dgo;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr + Dinc) ] *
                                     CTF[*(dptr+Dinc) * 3] + 
                                     red_s_shade[ *(nptr + Dinc) ] );
        green_shaded_value += weight * ( green_d_shade[ *(nptr + Dinc) ] *
                                     CTF[*(dptr+Dinc) * 3 + 1] + 
                                     green_s_shade[ *(nptr + Dinc) ] );
        blue_shaded_value  += weight * ( blue_d_shade[ *(nptr + Dinc) ] *
                                     CTF[*(dptr+Dinc) * 3 + 2] +
                                     blue_s_shade[ *(nptr + Dinc) ] );
        }
      
      if ( E )
        {
        weight = t1*t2*z * E * Ego;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr + Einc) ] *
                                     CTF[*(dptr+Einc) * 3] + 
                                     red_s_shade[ *(nptr + Einc) ] );
        green_shaded_value += weight * ( green_d_shade[ *(nptr + Einc) ] *
                                     CTF[*(dptr+Einc) * 3 + 1] + 
                                     green_s_shade[ *(nptr + Einc) ] );
        blue_shaded_value  += weight * ( blue_d_shade[ *(nptr + Einc) ] *
                                     CTF[*(dptr+Einc) * 3 + 2] +
                                     blue_s_shade[ *(nptr + Einc) ] );
        }
      
      if ( F )
        {
        weight = x*z*t2 * F * Fgo;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr + Finc) ] *
                                     CTF[*(dptr+Finc) * 3] + 
                                     red_s_shade[ *(nptr + Finc) ] );
        green_shaded_value += weight * ( green_d_shade[ *(nptr + Finc) ] *
                                     CTF[*(dptr+Finc) * 3 + 1] + 
                                     green_s_shade[ *(nptr + Finc) ] );
        blue_shaded_value  += weight * ( blue_d_shade[ *(nptr + Finc) ] *
                                     CTF[*(dptr+Finc) * 3 + 2] +
                                     blue_s_shade[ *(nptr + Finc) ] );
        }
      
      if ( G )
        {
        weight = t1*y*z * G * Ggo;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr + Ginc) ] *
                                     CTF[*(dptr+Ginc) * 3] + 
                                     red_s_shade[ *(nptr + Ginc) ] );
        green_shaded_value += weight * ( green_d_shade[ *(nptr + Ginc) ] *
                                     CTF[*(dptr+Ginc) * 3 + 1] + 
                                     green_s_shade[ *(nptr + Ginc) ] );
        blue_shaded_value  += weight * ( blue_d_shade[ *(nptr + Ginc) ] *
                                     CTF[*(dptr+Ginc) * 3 + 2] +
                                     blue_s_shade[ *(nptr + Ginc) ] );
      } 
      
      if ( H )
        {
        weight = x*z*y * H * Hgo;
        opacity += weight;
        red_shaded_value   += weight * ( red_d_shade[ *(nptr + Hinc) ] *
                                     CTF[*(dptr+Hinc) * 3] + 
                                     red_s_shade[ *(nptr + Hinc) ] );
        green_shaded_value += weight * ( green_d_shade[ *(nptr + Hinc) ] *
                                     CTF[*(dptr+Hinc) * 3 + 1] + 
                                     green_s_shade[ *(nptr + Hinc) ] );
        blue_shaded_value  += weight * ( blue_d_shade[ *(nptr + Hinc) ] *
                                     CTF[*(dptr+Hinc) * 3 + 2] +
                                     blue_s_shade[ *(nptr + Hinc) ] );
        }
      
      // Accumulate intensity and opacity for this sample location   
      accum_red_intensity   += red_shaded_value   * remaining_opacity;
      accum_green_intensity += green_shaded_value * remaining_opacity;
      accum_blue_intensity  += blue_shaded_value  * remaining_opacity;
      remaining_opacity *= (1.0 - opacity);
      
      // Increment our position and compute our voxel location
      ray_position[0] += ray_increment[0];
      ray_position[1] += ray_increment[1];
      ray_position[2] += ray_increment[2];      
      voxel[0] = vtkFloorFuncMacro( ray_position[0] );
      voxel[1] = vtkFloorFuncMacro( ray_position[1] );
      voxel[2] = vtkFloorFuncMacro( ray_position[2] );
      }
    }

  // Cap the accumulated intensity at 1.0
  if ( accum_red_intensity > 1.0 )
    {
    accum_red_intensity = 1.0;
    }
  if ( accum_green_intensity > 1.0 )
    {
    accum_green_intensity = 1.0;
    }
  if ( accum_blue_intensity > 1.0 )
    {
    accum_blue_intensity = 1.0;
    }
  
  if( remaining_opacity < VTK_REMAINING_OPACITY )
    {
    remaining_opacity = 0.0;
    }

  // Set the return pixel value.  The depth value is the distance to the
  // center of the volume.
  dynamicInfo->Color[0] = accum_red_intensity;
  dynamicInfo->Color[1] = accum_green_intensity;
  dynamicInfo->Color[2] = accum_blue_intensity;
  dynamicInfo->Color[3] = 1.0 - remaining_opacity;
  dynamicInfo->NumberOfStepsTaken = steps_this_ray;


}

// Constructor for the vtkVolumeRayCastCompositeFunction class
vtkVolumeRayCastCompositeFunction::vtkVolumeRayCastCompositeFunction()
{
  this->CompositeMethod = VTK_COMPOSITE_INTERPOLATE_FIRST;
}

// Destruct the vtkVolumeRayCastCompositeFunction
vtkVolumeRayCastCompositeFunction::~vtkVolumeRayCastCompositeFunction()
{
}

// This is called from RenderAnImage (in vtkDepthPARCMapper.cxx)
// It uses the integer data type flag that is passed in to
// determine what type of ray needs to be cast (which is handled
// by a templated function.  It also uses the shading and
// interpolation types to determine which templated function
// to call.
void vtkVolumeRayCastCompositeFunction::CastRay( vtkVolumeRayCastDynamicInfo *dynamicInfo,
                                                 vtkVolumeRayCastStaticInfo *staticInfo )
{
  void *data_ptr;
  data_ptr = staticInfo->ScalarDataPointer;

  // Cast the ray for the data type and shading/interpolation type
  if ( staticInfo->InterpolationType == VTK_NEAREST_INTERPOLATION )
    {
    if ( staticInfo->Shading == 0 )
      {
      // Nearest neighbor and no shading
      switch ( staticInfo->ScalarDataType )
        {
        case VTK_UNSIGNED_CHAR:
          vtkCastRay_NN_Unshaded( (unsigned char *)data_ptr, dynamicInfo, 
                                  staticInfo );
          break;
        case VTK_UNSIGNED_SHORT:
          vtkCastRay_NN_Unshaded( (unsigned short *)data_ptr, dynamicInfo, 
                                  staticInfo );
          break;
        default:
          vtkWarningMacro ( << "Unsigned char and unsigned short are the only supported datatypes for rendering" );
          break;
        }
      }
    else
      {
      // Nearest neighbor and shading
      switch ( staticInfo->ScalarDataType )
        {
        case VTK_UNSIGNED_CHAR:
          vtkCastRay_NN_Shaded( (unsigned char *)data_ptr, dynamicInfo, staticInfo);
          break;
        case VTK_UNSIGNED_SHORT:
          vtkCastRay_NN_Shaded( (unsigned short *)data_ptr, dynamicInfo, staticInfo);
          break;
        default:
          vtkWarningMacro ( << "Unsigned char and unsigned short are the only supported datatypes for rendering" );
          break;
        }
      }
    }
  else 
    {
    // Trilinear interpolation and no shading
    if ( staticInfo->Shading == 0 )
      {
      if ( this->CompositeMethod == VTK_COMPOSITE_INTERPOLATE_FIRST )
        {
        switch ( staticInfo->ScalarDataType )
          {
          case VTK_UNSIGNED_CHAR:
           vtkCastRay_TrilinSample_Unshaded( (unsigned char *)data_ptr,  
                                              dynamicInfo, staticInfo );
            break;
          case VTK_UNSIGNED_SHORT:
            vtkCastRay_TrilinSample_Unshaded( (unsigned short *)data_ptr, 
                                              dynamicInfo, staticInfo );
            break;
          default:
            vtkWarningMacro ( << "Unsigned char and unsigned short are the only supported datatypes for rendering" );
            break;
          }
        }
      else
        {
        switch ( staticInfo->ScalarDataType )
          {
          case VTK_UNSIGNED_CHAR:
            vtkCastRay_TrilinVertices_Unshaded( (unsigned char *)data_ptr,  
                                                dynamicInfo, staticInfo );
            break;
          case VTK_UNSIGNED_SHORT:
            vtkCastRay_TrilinVertices_Unshaded( (unsigned short *)data_ptr, 
                                                dynamicInfo, staticInfo );
            break;
          default:
            vtkWarningMacro ( << "Unsigned char and unsigned short are the only supported datatypes for rendering" );
            break;
          }
        }
      } 
    else
      {
      // Trilinear interpolation and shading
      if ( this->CompositeMethod == VTK_COMPOSITE_INTERPOLATE_FIRST )
        {
        switch ( staticInfo->ScalarDataType )
          {
          case VTK_UNSIGNED_CHAR:
            vtkCastRay_TrilinSample_Shaded( (unsigned char *)data_ptr, 
                                            dynamicInfo, staticInfo );
            break;
          case VTK_UNSIGNED_SHORT:
            vtkCastRay_TrilinSample_Shaded( (unsigned short *)data_ptr, 
                                            dynamicInfo, staticInfo );
            break;
          default:
            vtkWarningMacro ( << "Unsigned char and unsigned short are the only supported datatypes for rendering" );
            break;
          }
        }       
      else
        {
        switch ( staticInfo->ScalarDataType )
          {
          case VTK_UNSIGNED_CHAR:
            vtkCastRay_TrilinVertices_Shaded( (unsigned char *)data_ptr, 
                                              dynamicInfo, staticInfo );
            break;
          case VTK_UNSIGNED_SHORT:
            vtkCastRay_TrilinVertices_Shaded( (unsigned short *)data_ptr, 
                                              dynamicInfo, staticInfo );
            break;
          default:
            vtkWarningMacro ( << "Unsigned char and unsigned short are the only supported datatypes for rendering" );
            break;
          }
        }
      }
    }
}

float vtkVolumeRayCastCompositeFunction::GetZeroOpacityThreshold( vtkVolume 
                                                                  *vol )
{
  return vol->GetProperty()->GetScalarOpacity()->GetFirstNonZeroValue();
}

// We don't need to do any specific initialization here...
void vtkVolumeRayCastCompositeFunction::SpecificFunctionInitialize( 
                                vtkRenderer *vtkNotUsed(ren), 
                                vtkVolume *vtkNotUsed(vol),
                                vtkVolumeRayCastStaticInfo *vtkNotUsed(staticInfo),
                                vtkVolumeRayCastMapper *vtkNotUsed(mapper) )
{
}

// Description:
// Return the composite method as a descriptive character string.
const char *vtkVolumeRayCastCompositeFunction::GetCompositeMethodAsString(void)
{
  if( this->CompositeMethod == VTK_COMPOSITE_INTERPOLATE_FIRST )
    {
    return "Interpolate First";
    }
  if( this->CompositeMethod == VTK_COMPOSITE_CLASSIFY_FIRST )
    {
    return "Classify First";
    }
  else
    {
    return "Unknown";
    }
}

// Print method for vtkVolumeRayCastCompositeFunction
// Since there is nothing local to print, just print the object stuff.
void vtkVolumeRayCastCompositeFunction::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "Composite Method: " << this->GetCompositeMethodAsString()
     << "\n";

}





