#include <stdio.h>
#include <stdlib.h>  // for atoi
#include <string.h>


#include <png.h>
#include <setjmp.h>   // note: this must be included *after* png.h
#include<stdint.h>

static  png_infop info = NULL;
static png_structp png = NULL;

static void PNGAPI error_function(png_structp png, png_const_charp dummy) 
{
  	longjmp(png_jmpbuf(png), 1);
}

static int WritePNG(FILE* out_file, unsigned char* rgb,  png_uint_32 width, png_uint_32 height) 
{
  	int y;
	int stride = 3*width;
	printf("\nHeight = %u Width = %u\n", height, width);
  	png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, error_function, NULL);
  	if (png == NULL) 
	{
    		return 0;
  	}
  	info = png_create_info_struct(png);
  	if (info == NULL) 
	{
   	 	png_destroy_write_struct(&png, NULL);
    		return 0;
 	}
  	if (setjmp(png_jmpbuf(png))) 
	{
   		 png_destroy_write_struct(&png, &info);
    		return 0;
  	}
        png_init_io(png, out_file);
  	png_set_IHDR(png, info, width, height, 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
        png_write_info(png, info);
	
    	for (y = 0; y < height; y++) 
	{
		png_bytep row = rgb + y * stride;
    		png_write_rows(png, &row, 1);
  	}
 	return 1;
}


int main(int argc, char *argv[])
{
	uint8_t *BinF;
	uint8_t *RGB;
	uint8_t R, G, B, A, r, g, b;
	uint32_t ht, wt;
	FILE *fp, *outfile;
	FILE *txtF;
	txtF = fopen("ImgSz.txt", "r");
	if(txtF == NULL)
	{
		printf("\nUnable to open file dimension file...");
	}
	else
	{
		fscanf(txtF, "%d %d", &ht, &wt);
	}
	fclose(txtF);
	BinF = (uint8_t *) malloc(ht*wt*4);
	RGB =  (uint8_t *) malloc(ht*wt*3);

 	outfile = fopen(argv[2], "wb");
	if(outfile == NULL)
	{
		printf("\nError in opening RGB file ...\n");
		return 0;
	}

	fp=fopen(argv[1], "rb");
	if(fp == NULL)
	{
		printf("\nUnable to open binary file ...\n");
		return 0;
	}
	fread(BinF, 1, ht*wt*4, fp);	
	//RGBA
	int i, j;
	for(i=0, j = ht*wt - 1; i<ht*wt; i++, j--)
	{
		R = BinF[i*4];
		G = BinF[i*4+1];
		B = BinF[i*4+2];
		A = BinF[i*4+3];
		r = ( (255.0 - A) / 255.0) * 255 + ( (float) A / 255.0 )*R;
		g =  ( (255.0 - A) / 255.0) * 255 + ( (float) A / 255.0 )*G;
		b =  ( (255.0 - A) / 255.0) * 255 + ( (float) A / 255.0 )*B;
		//printf("\n R = %d G = %d B = %d A = %d r = %d g = %d b = %d", R, G, B, A, r , g ,b);
		RGB[j*3] = r;
		RGB[j*3+1] = g;
		RGB[j*3+2] = b;
	}
	WritePNG(outfile, RGB,  wt, ht);
	fclose(fp);
	fclose(outfile);
	return 0;
}
