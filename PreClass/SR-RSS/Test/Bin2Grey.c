#include <stdio.h>
#include <stdlib.h>  // for atoi
#include <string.h>
#include <png.h>
#include <setjmp.h>   // note: this must be included *after* png.h
#include <stdint.h>
#include<math.h>

uint8_t* rgb = NULL;
uint32_t GHeight = 0;
uint32_t GWidth = 0;

int stride;
static  png_infop info = NULL;
static png_structp png = NULL;

//===============================================================================================================================================
//*******************************************************   JPEG/PNG READ/WRITE FUNCTIONS   *****************************************************
//===============================================================================================================================================

static void PNGAPI error_function(png_structp png, png_const_charp dummy) 
{
  	longjmp(png_jmpbuf(png), 1);
}
static int WritePNG_Gray(FILE* out_file, uint8_t* rgb1,  png_uint_32 width, png_uint_32 height) 
{
	int i, j;
	int y;
	uint8_t *rgb;
	rgb = rgb1;
	png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, error_function, NULL);
  	if (png == NULL) 
	{
    		return 0;
  	}
  	info = png_create_info_struct(png);
  	if (info == NULL) 
	{
   	 	png_destroy_write_struct(&png, NULL);
    		return 0;
 	}
  	if (setjmp(png_jmpbuf(png))) 
	{
   		 png_destroy_write_struct(&png, &info);
    		return 0;
  	}
        png_init_io(png, out_file);
  	png_set_IHDR(png, info, width, height, 8, PNG_COLOR_TYPE_GRAY, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
        png_write_info(png, info);	
    	for (y = 0; y < height; y++) 
	{
		png_bytep row = rgb1 + y * (stride);
    		png_write_rows(png, &row, 1);
	}
 	return 1;
}

//=============================================================================================================================================================
//***************************************************************   MAIN FUNCTION   ***************************************************************************
//=============================================================================================================================================================

int main (int argc, char *argv[])
{
	//Usage BinF PNGF HEIGH WIDTH

	FILE *infile = NULL, *outfile = NULL;
		
	FILE *txtF;
	txtF = fopen("ImgSz.txt", "r");
	if(txtF == NULL)
	{
		printf("\nUnable to open file dimension file...");
	}
	else
	{
		fscanf(txtF, "%d %d", &GHeight, &GWidth);
	}
	fclose(txtF);

	infile = fopen(argv[1], "rb");
	outfile = fopen(argv[2], "wb");
	int i;
	uint8_t a, b, t;

	uint8_t *cont;
	cont = (uint8_t*) malloc(sizeof(uint8_t) * GHeight * GWidth * 2);
	rgb = (uint8_t*) malloc(sizeof(uint8_t) * GHeight * GWidth);
	fread(cont, 1, GHeight * GWidth * 2, infile);
	float fA;
	for(i=0; i<GWidth*GHeight; i++)
	{
		//rgb[i] = (uint8_t) (((1 - fA) * cont[i]) + (fA * 255));
		//printf(" [ %u - %u ]", cont[2*i], cont[2*i+1]);	
		rgb[i] = ( (255.0 - cont[2*i+1]) / 255.0) * 255 + ( (float) cont[2*i+1] / 255.0 )*cont[2*i];

	}
	for(i=0; i<(GWidth*GHeight)/2; i++)
	{
		t = rgb[i];
		rgb[i] = rgb[GWidth*GHeight - 1 - i];
		rgb[GWidth*GHeight - 1 - i] = t;
	}


	stride = GWidth;
	WritePNG_Gray(outfile,rgb,GWidth,GHeight);	
	return 0;        
	
}

